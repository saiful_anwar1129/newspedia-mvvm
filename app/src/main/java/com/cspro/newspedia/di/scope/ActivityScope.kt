package com.cspro.newspedia.di.scope

import javax.inject.Scope

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

@kotlin.annotation.MustBeDocumented
@Scope
@kotlin.annotation.Retention
annotation class ActivityScope