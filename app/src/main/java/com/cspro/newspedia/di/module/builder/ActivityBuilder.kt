package com.cspro.newspedia.di.module.builder

import com.cspro.newspedia.ui.detail.DetailNewsActivity
import com.cspro.newspedia.ui.detail.DetailNewsActivityModule
import com.cspro.newspedia.ui.headline.HeadlineFragment
import com.cspro.newspedia.ui.headline.HeadlineFragmentModule
import com.cspro.newspedia.ui.headline.HeadlineFragmentProvider
import com.cspro.newspedia.ui.list.ListNewsActivity
import com.cspro.newspedia.ui.list.ListNewsActivityModule
import com.cspro.newspedia.ui.main.MainActivity
import com.cspro.newspedia.ui.main.MainActivityModule
import com.cspro.newspedia.ui.main.browse.BrowseFragmentProvider
import com.cspro.newspedia.ui.main.setting.SettingFragmentProvider
import com.cspro.newspedia.ui.main.topheadlines.TopHeadlinesFragmentProvider
import com.cspro.newspedia.ui.search.SearchNewsActivity
import com.cspro.newspedia.ui.search.SearchNewsActivityModule
import com.cspro.newspedia.ui.splash.SplashActivity
import com.cspro.newspedia.ui.splash.SplashActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [
        MainActivityModule::class,
        TopHeadlinesFragmentProvider::class,
        BrowseFragmentProvider::class,
        SettingFragmentProvider::class
    ])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(SearchNewsActivityModule::class)])
    abstract fun bindSearchActivity(): SearchNewsActivity

    @ContributesAndroidInjector(modules = [(DetailNewsActivityModule::class)])
    abstract fun bindDetailNewsActivity(): DetailNewsActivity

    @ContributesAndroidInjector(modules = [ListNewsActivityModule::class, HeadlineFragmentProvider::class])
    abstract fun bindListNewsActivity(): ListNewsActivity
}