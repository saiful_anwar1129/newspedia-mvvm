package com.cspro.newspedia.di.module

import android.app.Application
import com.cspro.newspedia.di.qualifiers.InterceptorInfo
import com.cspro.newspedia.BuildConfig
import com.cspro.newspedia.app.AppConst
import com.cspro.newspedia.data.network.api.ApiHelper
import com.cspro.newspedia.data.network.api.AppApiHelper

import com.cspro.newspedia.data.network.api.topheadlines.TopHeadlinesApi
import com.cspro.newspedia.data.network.api.topheadlines.TopHeadlinesAppApi
import com.cspro.newspedia.data.network.remote.BaseUrl
import com.cspro.newspedia.data.network.remote.DefaultInterceptor
import com.cspro.newspedia.data.remote.everything.EverythingApi
import com.cspro.newspedia.data.remote.everything.EverythingAppApi
import com.cspro.newspedia.data.remote.source.SourceApi
import com.cspro.newspedia.data.remote.source.SourceAppApi
import com.cspro.newspedia.di.qualifiers.ApiKeyInfo
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

@Module
class NetworkModule {

    companion object {
        val API_DATE_FORMAT = "yyyy-MM-dd:HH"
    }

    @Singleton
    @Provides
    internal fun provideGson(): Gson = GsonBuilder().setDateFormat(API_DATE_FORMAT).create()

    @Singleton
    @Provides
    internal fun provideCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Singleton
    @Provides
    internal fun provideStethoInterceptor(): StethoInterceptor? = if (BuildConfig.DEBUG) StethoInterceptor() else null

    @Singleton
    @Provides
    internal fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor? = if (BuildConfig.DEBUG) HttpLoggingInterceptor() else null

    @Provides
    @Singleton
    internal fun provideDefaultInterceptor(@ApiKeyInfo apiKey: String): DefaultInterceptor = DefaultInterceptor(apiKey)

    @Provides
    @InterceptorInfo
    internal fun provideRetryCount(): Int {
        return AppConst.RETRY_NETWORK_REQUEST_COUNT
    }

    @Provides
    @Singleton
    internal fun provideRetryInterceptor(@InterceptorInfo retryCount: Int) : Interceptor {
        return Interceptor {
                chain ->
            val request = chain.request()
            var response: Response? = null
            var exception: IOException? = null

            var tryCount = 0
            while (tryCount < retryCount && (null == response || !response.isSuccessful)) {
                try {
                    response = chain.proceed(request)
                } catch (e: IOException) {
                    exception = e
                } finally {
                    tryCount++
                }
            }

            if (null == response && null != exception) {
                throw exception
            }

            response
        }
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(cache: Cache,
                            retryInterceptor: Interceptor,
                            httpLoggingInterceptor: HttpLoggingInterceptor?,
                            stethoInterceptor: StethoInterceptor?,
                            defaultInterceptor: DefaultInterceptor): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()

        stethoInterceptor?.let { okHttpClientBuilder.addNetworkInterceptor(it) }
        httpLoggingInterceptor?.let {
            okHttpClientBuilder.addInterceptor(it)
            it.setLevel(HttpLoggingInterceptor.Level.BODY)
        }

        okHttpClientBuilder.addNetworkInterceptor(defaultInterceptor)

        okHttpClientBuilder.addInterceptor(retryInterceptor)
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(300, TimeUnit.SECONDS)
            .writeTimeout(100, TimeUnit.SECONDS)
            .cache(cache)

        return okHttpClientBuilder.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BaseUrl.getVersion())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper

    @Provides
    @Singleton
    internal fun provideTopHeadlinesApi(topHeadlinesAppApi: TopHeadlinesAppApi) : TopHeadlinesApi = topHeadlinesAppApi

    @Provides
    @Singleton
    internal fun provideEverythingApi(everythingAppApi: EverythingAppApi) : EverythingApi = everythingAppApi

    @Provides
    @Singleton
    internal fun provideSourceApi(sourceAppApi: SourceAppApi) : SourceApi = sourceAppApi

}