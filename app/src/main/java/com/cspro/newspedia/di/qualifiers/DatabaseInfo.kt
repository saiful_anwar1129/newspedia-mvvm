package com.cspro.newspedia.di.qualifiers

import javax.inject.Qualifier

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    01/07/2018.
 */

@Qualifier
@Retention annotation class DatabaseInfo