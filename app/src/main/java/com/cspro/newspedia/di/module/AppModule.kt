package com.cspro.newspedia.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.cspro.newspedia.BuildConfig
import com.cspro.newspedia.app.AppConst
import com.cspro.newspedia.data.AppDataManager
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.data.local.db.AppDatabase
import com.cspro.newspedia.data.local.db.AppDbHelper
import com.cspro.newspedia.data.local.db.DbHelper
import com.cspro.newspedia.data.local.prefs.AppPreferenceHelper
import com.cspro.newspedia.data.local.prefs.PreferenceHelper
import com.cspro.newspedia.di.qualifiers.ApiKeyInfo
import com.cspro.newspedia.di.qualifiers.DatabaseInfo
import com.cspro.newspedia.di.qualifiers.PreferenceInfo
import com.cspro.newspedia.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @ApiKeyInfo
    internal fun provideApiKey(): String = BuildConfig.API_KEY

    @Provides
    @PreferenceInfo
    internal fun provideprefFileName(): String = AppConst.PREF_NAME

    @Provides
    @DatabaseInfo
    internal fun provideDatabaseName() : String = AppConst.DB_NAME

    @Provides
    @Singleton
    internal fun provideAppDatabase(@DatabaseInfo dbName: String, context: Context) =
        Room.databaseBuilder(context, AppDatabase::class.java, dbName).fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    internal fun provideDbHelper(appDbHelper: AppDbHelper) : DbHelper = appDbHelper

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    internal fun provideDataManager(appDataManager: AppDataManager) : DataManager = appDataManager

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()
}