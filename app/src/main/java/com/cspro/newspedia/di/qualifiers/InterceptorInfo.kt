package com.cspro.newspedia.di.qualifiers

import javax.inject.Qualifier

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

@Qualifier
@Retention annotation class InterceptorInfo
