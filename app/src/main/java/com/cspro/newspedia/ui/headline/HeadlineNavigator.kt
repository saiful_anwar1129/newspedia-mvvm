package com.cspro.newspedia.ui.headline

import com.cspro.newspedia.ui.base.BaseNavigator


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/29/2019.
 */
interface HeadlineNavigator : BaseNavigator {

    fun openDetailNewsActivity(articleUrl: String?, articleTitle: String?)

}