package com.cspro.newspedia.ui.base

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class BaseEmptyItemViewModel(private val listenerBase: BaseEmptyItemViewModelListener) {

    fun onRetryClick() = listenerBase.onRetryClick()

    interface BaseEmptyItemViewModelListener {
        fun onRetryClick()
    }
}