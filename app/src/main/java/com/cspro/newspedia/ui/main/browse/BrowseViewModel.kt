package com.cspro.newspedia.ui.main.browse

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.data.model.api.response.ListSourcesResponse
import com.cspro.newspedia.data.network.models.Source
import com.cspro.newspedia.ui.base.BaseNavigator
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.utils.CallbackWrapper
import com.cspro.newspedia.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class BrowseViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<BrowseNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable) {
    private val sourceList: ObservableList<Source>
    private val sourceListLiveData: MutableLiveData<List<Source>>

    init {
        this.sourceList = ObservableArrayList<Source>()
        this.sourceListLiveData = MutableLiveData()
    }
    
    fun fetchSourceList() {
        
        if (navigator.isNetworkConnected()) {
            setIsLoading(true)
            compositeDisposable.add(dataManager.getSources()
                .compose(schedulerProvider.ioToMainObservableScheduler())
                .doOnTerminate {
                    setIsLoading(false)
                    navigator.onRefreshStop()
                }
                .subscribeWith(object : CallbackWrapper<ListSourcesResponse>(navigator) {
                    override fun onSuccess(response: ListSourcesResponse) {
                        response.data?.let { sourceListLiveData.value = it }
                    }
                }))
        } else navigator.onConnectionError()
        
    }

    fun addItemsToSource(list: List<Source>) {
        sourceList.clear()
        sourceList.addAll(list)
    }

    fun getSourceList() : ObservableList<Source> = sourceList

    fun getSourceListLiveData() : MutableLiveData<List<Source>> = sourceListLiveData

}
