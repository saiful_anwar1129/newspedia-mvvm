package com.cspro.newspedia.ui.main.browse

import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Module
abstract class BrowseFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(BrowseFragmentModule::class))
    abstract fun provideBrowseFragmentFactory() : BrowseFragment

}