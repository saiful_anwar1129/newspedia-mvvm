package com.cspro.newspedia.ui.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.cspro.newspedia.ui.base.BaseActivity
import com.cspro.newspedia.BR
import com.cspro.newspedia.R
import com.cspro.newspedia.app.AppConst
import com.cspro.newspedia.databinding.ActivityListNewsBinding
import com.cspro.newspedia.ui.detail.DetailNewsIntent
import com.cspro.newspedia.ui.headline.HeadlinePagerAdapter
import com.cspro.newspedia.ui.main.topheadlines.TopHeadlinesAdapter
import com.cspro.newspedia.ui.search.SearchIntent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class ListNewsActivity : BaseActivity<ActivityListNewsBinding, ListNewsViewModel>(), ListNewsNavigator,
    SwipeRefreshLayout.OnRefreshListener, TopHeadlinesAdapter.TopHeadlinesAdapterListener, HasSupportFragmentInjector {

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var topHeadlinesAdapter: TopHeadlinesAdapter
    @Inject
    internal lateinit var headlinePagerAdapter: HeadlinePagerAdapter
    
    internal lateinit var listNewsViewModel: ListNewsViewModel
    private lateinit var activityListNewsBinding: ActivityListNewsBinding
    private lateinit var sourceId: String
    private lateinit var sourceName: String

    private var countPage = 0

    private lateinit var adsHandler: Handler
    private val adsRunnable = object : Runnable {
        override fun run() {
            headlinePagerAdapter?.let { adapter ->
                if (headlinePagerAdapter.count > 0) {
                    countPage++
                    if (countPage == adapter.count) {
                        countPage = 0
                    }
                    if (countPage < adapter.count) {
                        activityListNewsBinding?.let {
                            it.headlineViewPager.setCurrentItem(countPage, true)
                        }
                    }
                    adsHandler.postDelayed(this, AppConst.TIMES_LOOP_PROMO)
                }
            }
        }
    }
    
    override fun getLayoutId(): Int = R.layout.activity_list_news

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getViewModel(): ListNewsViewModel {
        listNewsViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ListNewsViewModel::class.java)
        return listNewsViewModel
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adsHandler = Handler()
    }

    override fun onResume() {
        if (adsHandler != null) {
            adsHandler.postDelayed(adsRunnable, AppConst.TIMES_LOOP_PROMO);
        }
        super.onResume()
    }

    override fun onStop() {
        adsHandler.removeCallbacks(adsRunnable);
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when(it.itemId) {
                R.id.searchMenu -> {
                    openSearchActivity()
                    return true
                }
                else -> {
                    return super.onOptionsItemSelected(item)
                }
            }
        }
    }

    override fun openSearchActivity() {
        startActivity(SearchIntent(sourceId = sourceId))
        overridePendingTransition(R.anim.slide_animation_enter, R.anim.nothing)
    }

    override fun onActivityReady(savedInstanceState: Bundle?) {
        sourceId = intent.getStringExtra(PARAM_SOURCE_ID)
        sourceName = intent.getStringExtra(PARAM_SOURCE_NAME)
        requireNotNull(sourceId) { "sourceId provided in Intent extras" }
        requireNotNull(sourceName) { "sourceName provided in Intent extras" }

        listNewsViewModel.navigator = this
        activityListNewsBinding = getViewDataBinding()

        listNewsViewModel.fetchData(sourceId)
        activityListNewsBinding?.let {
            setSupportActionBar(it.toolbar)
            it.toolbar.setTitle(sourceName)
            it.toolbar.setNavigationOnClickListener { onBackPressed() }
            supportActionBar?.also {
                it.setDisplayHomeAsUpEnabled(true)
                it.setDisplayShowHomeEnabled(true)
            }
            it.swipe.setOnRefreshListener(this)
            topHeadlinesAdapter.setListener(this)

            it.recyclerView.itemAnimator = DefaultItemAnimator()
            it.recyclerView.layoutManager = (LinearLayoutManager(this@ListNewsActivity).apply { orientation = LinearLayoutManager.VERTICAL })
            it.recyclerView.adapter = topHeadlinesAdapter

            it.headlineViewPager.adapter = headlinePagerAdapter
            it.headlineViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(p0: Int) {
                }

                override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                }

                override fun onPageSelected(p0: Int) {
                    countPage = p0;
                }
            })
        }

        subscribeToLiveData()
    }

    private fun subscribeToLiveData() {
        listNewsViewModel.getArticleListLiveData().observe(this, Observer {
            it?.let {
                listNewsViewModel.addItemsToArticle(it)

            }
        })

        listNewsViewModel.getArticleTopHeadlinesLiveData().observe(this, Observer {
            it?.let {
                adsHandler.removeCallbacks(adsRunnable);
                headlinePagerAdapter.setHeadlines(it)
                adsHandler.postDelayed(adsRunnable, AppConst.TIMES_LOOP_PROMO);
            }
        })
    }

    override fun onRefreshStop() {
        activityListNewsBinding?.let {
            if (it.swipe.isRefreshing) it.swipe.isRefreshing = false
        }
    }

    override fun onRefresh() {
        listNewsViewModel.fetchEverything(sourceId)
    }

    override fun onItemClick(articleUrl: String?, articleTitle: String?) {
        startActivity(DetailNewsIntent(articleUrl, articleTitle))
        overridePendingTransition(R.anim.slide_animation_enter, R.anim.nothing)
    }

    override fun onRetryClick() {
        listNewsViewModel.fetchEverything(sourceId)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.nothing, R.anim.slide_animation_leave)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentDispatchingAndroidInjector

}

val PARAM_SOURCE_ID = "PARAM_SOURCE_ID"
val PARAM_SOURCE_NAME = "PARAM_SOURCE_NAME"
fun Context.ListNewsIntent(sourceId: String?, name: String?) : Intent = Intent(this, ListNewsActivity::class.java).apply {
    putExtra(PARAM_SOURCE_ID, sourceId)
    putExtra(PARAM_SOURCE_NAME, name)
}