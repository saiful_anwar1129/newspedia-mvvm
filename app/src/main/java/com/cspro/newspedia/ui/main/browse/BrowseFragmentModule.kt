package com.cspro.newspedia.ui.main.browse

import android.arch.lifecycle.ViewModelProvider
import com.cspro.newspedia.app.ViewModelProviderFactory
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Module
class BrowseFragmentModule{

    @Provides
    internal fun provideBrowseViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : BrowseViewModel =
        BrowseViewModel(dataManager, schedulerProvider, compositeDisposable)

    @Provides
    internal fun browseViewModelProvider(viewModel: BrowseViewModel) : ViewModelProvider.Factory =
        ViewModelProviderFactory(viewModel)

    @Provides
    internal fun provideSourceAdapter() : SourceAdapter = SourceAdapter(ArrayList())

}