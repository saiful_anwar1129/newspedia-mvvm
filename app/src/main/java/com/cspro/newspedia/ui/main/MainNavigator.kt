package com.cspro.newspedia.ui.main

import com.cspro.newspedia.ui.base.BaseNavigator


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
interface MainNavigator : BaseNavigator {

    fun openSearchActivity()

}