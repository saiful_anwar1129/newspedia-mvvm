package com.cspro.newspedia.ui.search

import android.support.v7.widget.RecyclerView
import com.cspro.newspedia.ui.base.BaseViewHolder
import com.cspro.newspedia.app.AppLogger
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.databinding.ListItemEmptyBinding
import com.cspro.newspedia.databinding.ListItemSearchViewBinding
import com.cspro.newspedia.ui.base.BaseEmptyItemViewModel
import java.lang.RuntimeException


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class SearchNewsAdapter constructor(val articleListItems: MutableList<Article>) : RecyclerView.Adapter<BaseViewHolder>() {

    private lateinit var listener: SearchAdapterListener

    fun clearItems() {
        articleListItems.clear()
    }

    fun addItems(list: MutableList<Article>) {
        articleListItems.addAll(list)
        notifyDataSetChanged()
    }

    fun addItem(item: Article) {
        articleListItems.add(item)
        notifyDataSetChanged()
    }

    fun setListener(listener: SearchAdapterListener) {
        this.listener = listener
    }

    override fun getItemCount(): Int = articleListItems.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val listItemSearchBinding = ListItemSearchViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false)
        return SearchViewHolder(listItemSearchBinding)
    }

    inner class SearchViewHolder(private val binding: ListItemSearchViewBinding) : BaseViewHolder(binding.root),
        SearchItemViewModel.SearchItemViewModelListener {

        private lateinit var searchItemViewModel: SearchItemViewModel

        override fun onBind(position: Int) {
            val article = articleListItems[position]
            searchItemViewModel = SearchItemViewModel(article, this)
            binding.viewModel = searchItemViewModel

            binding.executePendingBindings()
        }

        override fun onItemClick(articleUrl: String?, articleTitle: String?) {
            listener?.let { it.onItemClick(articleUrl, articleTitle) }
        }
    }

    interface SearchAdapterListener {
        fun onRetryClick()

        fun onItemClick(articleUrl: String?, articleTitle: String?)
    }
}