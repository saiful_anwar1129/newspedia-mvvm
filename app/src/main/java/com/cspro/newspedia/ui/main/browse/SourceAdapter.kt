package com.cspro.newspedia.ui.main.browse

import android.support.v7.widget.RecyclerView
import com.cspro.newspedia.databinding.ListItemEmptyBinding
import com.cspro.newspedia.databinding.ListItemSourcesBinding
import com.cspro.newspedia.ui.base.BaseViewHolder
import android.view.LayoutInflater
import android.view.ViewGroup
import com.cspro.newspedia.data.network.models.Source
import com.cspro.newspedia.ui.base.BaseEmptyItemViewModel
import java.lang.RuntimeException


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class SourceAdapter constructor(val sourceListItems: MutableList<Source>) : RecyclerView.Adapter<BaseViewHolder>() {

    companion object {
        val VIEW_TYPE_EMPTY = 0
        val VIEW_TYPE_NORMAL = 1
    }

    private lateinit var listener: SourceAdapterListener

    override fun getItemViewType(position: Int): Int {
        return if (sourceListItems.isNotEmpty()) VIEW_TYPE_NORMAL
        else VIEW_TYPE_EMPTY
    }

    fun clearItems() {
        sourceListItems.clear()
    }

    fun addItems(list: MutableList<Source>) {
        sourceListItems.addAll(list)
        notifyDataSetChanged()
    }

    fun addItem(item: Source) {
        sourceListItems.add(item)
        notifyDataSetChanged()
    }

    fun setListener(listener: SourceAdapterListener) {
        this.listener = listener
    }

    override fun getItemCount(): Int = if (sourceListItems.size > 0) sourceListItems.size else 1

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder = when(viewType) {
        SourceAdapter.VIEW_TYPE_NORMAL -> {
            val listItemSourceBinding = ListItemSourcesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false)
            SourceViewHolder(listItemSourceBinding)
        }
        SourceAdapter.VIEW_TYPE_EMPTY -> {
            val listItemEmptyBinding = ListItemEmptyBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false)
            EmptyViewHolder(listItemEmptyBinding)
        }
        else -> throw RuntimeException("The type is Empty and Normal")
    }

    inner class SourceViewHolder(private val binding: ListItemSourcesBinding) : BaseViewHolder(binding.root),
        SourceItemViewModel.SourceItemViewModelListener{

        private lateinit var sourceItemViewModel: SourceItemViewModel

        override fun onBind(position: Int) {
            val source = sourceListItems[position]
            sourceItemViewModel = SourceItemViewModel(source, this)
            binding.viewModel = sourceItemViewModel

            binding.executePendingBindings()
        }

        override fun onItemClick(sourceId: String?, name: String?) {
            listener?.let { it.onItemClick(sourceId, name) }
        }
    }

    inner class EmptyViewHolder(private val binding: ListItemEmptyBinding) : BaseViewHolder(binding.root),
        BaseEmptyItemViewModel.BaseEmptyItemViewModelListener {

        override fun onBind(position: Int) {
            val emptyItemViewModel = BaseEmptyItemViewModel(this)
            binding.setViewModel(emptyItemViewModel)
        }

        override fun onRetryClick() {
            listener?.let { it.onRetryClick() }
        }
    }

    interface SourceAdapterListener {
        fun onRetryClick()

        fun onItemClick(sourceId: String?, name: String?)
    }
}