package com.cspro.newspedia.ui.main.topheadlines

import com.cspro.newspedia.ui.headline.HeadlineFragmentModule
import com.cspro.newspedia.ui.headline.HeadlineFragmentProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Module
abstract class TopHeadlinesFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(
        HeadlineFragmentProvider::class,
        TopHeadlinesFragmentModule::class
    ))
    abstract fun provideTopHeadlinesFragmentFactory() : TopHeadlinesFragment

}