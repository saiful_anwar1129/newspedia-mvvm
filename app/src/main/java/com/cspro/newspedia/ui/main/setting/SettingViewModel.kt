package com.cspro.newspedia.ui.main.setting

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.databinding.ObservableList
import com.cspro.newspedia.BuildConfig
import com.cspro.newspedia.app.AppLogger
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.data.model.db.Country
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import java.lang.RuntimeException
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class SettingViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<SettingNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable) {
    val versionName: ObservableField<String>
    private val countryModels: ObservableList<Country>
    private val countriesLiveData: MutableLiveData<List<Country>>

    init {
        this.versionName = ObservableField(BuildConfig.VERSION_NAME)
        this.countryModels = ObservableArrayList<Country>()
        this.countriesLiveData = MutableLiveData()
    }

    fun setUpSpinnerSelect(position: Int) {
        setIsLoading(true)

        compositeDisposable.add(getSources(countryModels[position])
            .compose(schedulerProvider.ioToMainObservableScheduler())
            .subscribe(
                {
                    setIsLoading(false)
                    navigator.successUpdate("Success to set")
                },
                { error ->
                    navigator.onUnknownError(error.message)
                    setIsLoading(false)
                }
            ))
    }

    fun fetchCountries() {
        setIsLoading(true)
        compositeDisposable.add(dataManager.loadAllCountries()
            .doOnTerminate { setIsLoading(false) }
            .compose(schedulerProvider.ioToMainObservableScheduler())
            .subscribe({
                countriesLiveData.value = it
            }, {error -> AppLogger.d("error: ${error.message}") }))
    }

    fun addCountryItemToList(countryItems: List<Country>) {
        countryModels.clear()
        countryModels.addAll(countryItems)
    }

    fun getCountryModels() : ObservableList<Country> = countryModels

    fun getCountriesLiveData(): MutableLiveData<List<Country>> = countriesLiveData

}
