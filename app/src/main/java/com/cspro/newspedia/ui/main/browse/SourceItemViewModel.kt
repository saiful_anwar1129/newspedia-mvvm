package com.cspro.newspedia.ui.main.browse

import android.databinding.ObservableField
import com.cspro.newspedia.data.network.models.Source


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class SourceItemViewModel constructor(source: Source, private val listener: SourceItemViewModelListener) {
    private val source: Source = source

    val name: ObservableField<String>
    val desc: ObservableField<String>
    val web: ObservableField<String>

    init {
        this.name = ObservableField(this.source.name)
        this.desc = ObservableField(this.source.description)
        this.web = ObservableField(this.source.url.replaceFirst("^(http[s]?://www\\.|http[s]?://|www\\.)",""))
    }

    fun onItemClick() = listener.onItemClick(sourceId = this.source.id, name = this.source.name)

    interface SourceItemViewModelListener {
        fun onItemClick(sourceId: String?, name: String?)
    }

}