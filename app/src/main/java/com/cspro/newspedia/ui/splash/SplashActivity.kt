package com.cspro.newspedia.ui.splash

import android.os.Bundle
import com.cspro.newspedia.R
import com.cspro.newspedia.BR
import com.cspro.newspedia.databinding.ActivitySplashBinding
import com.cspro.newspedia.ui.base.BaseActivity
import com.cspro.newspedia.ui.main.MainIntent
import javax.inject.Inject
import kotlin.concurrent.thread

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(), SplashNavigator {

    @Inject
    internal lateinit var splashViewModel: SplashViewModel

    override fun getLayoutId(): Int = R.layout.activity_splash

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getViewModel(): SplashViewModel = splashViewModel

    override fun onResume() {
        super.onResume()
        thread(start = true) {
            try {
                splashViewModel.setIsLoading(true)
                Thread.sleep(800L)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } finally {
                splashViewModel.startSeeding()
            }
        }
    }
    override fun onActivityReady(savedInstanceState: Bundle?) {
        splashViewModel.navigator = this
    }

    override fun openMainActivity() {
        startActivity(MainIntent())
        finish()
    }
}