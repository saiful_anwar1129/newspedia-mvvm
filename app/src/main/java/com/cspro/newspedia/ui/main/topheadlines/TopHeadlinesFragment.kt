package com.cspro.newspedia.ui.main.topheadlines

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.cspro.newspedia.BR
import com.cspro.newspedia.R
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.ui.base.BaseFragment
import com.cspro.newspedia.databinding.FragmentTopHeadlinesBinding
import com.cspro.newspedia.ui.detail.DetailNewsIntent
import com.cspro.newspedia.ui.headline.HeadlinePagerAdapter
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject
import android.os.Handler
import com.cspro.newspedia.app.AppConst
import com.cspro.newspedia.ui.main.MainActivity


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class TopHeadlinesFragment : BaseFragment<FragmentTopHeadlinesBinding, TopHeadlinesViewModel>(), TopHeadlinesNavigator,
    SwipeRefreshLayout.OnRefreshListener, TopHeadlinesAdapter.TopHeadlinesAdapterListener, HasSupportFragmentInjector {

    companion object {
        val TAG = TopHeadlinesFragment::class.java.simpleName

        fun newInstance() : TopHeadlinesFragment = TopHeadlinesFragment()
    }

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var topHeadlinesAdapter: TopHeadlinesAdapter
    @Inject
    internal lateinit var headlinePagerAdapter: HeadlinePagerAdapter

    private var fragmentTopHeadlinesBinding: FragmentTopHeadlinesBinding? = null
    internal lateinit var topHeadlinesViewModel: TopHeadlinesViewModel
    private var countPage = 0

    private lateinit var adsHandler: Handler
    private val adsRunnable = object : Runnable {
        override fun run() {
            headlinePagerAdapter?.let { adapter ->
                if (headlinePagerAdapter.count > 0) {
                    countPage++
                    if (countPage == adapter.count) {
                        countPage = 0
                    }
                    if (countPage < adapter.count) {
                        fragmentTopHeadlinesBinding?.let {
                          it.headlineViewPager.setCurrentItem(countPage, true)
                        }
                    }
                    adsHandler.postDelayed(this, AppConst.TIMES_LOOP_PROMO)
                }
            }
        }
    }

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.fragment_top_headlines

    override fun getViewModel(): TopHeadlinesViewModel {
        topHeadlinesViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TopHeadlinesViewModel::class.java)
        return topHeadlinesViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adsHandler = Handler()
        topHeadlinesViewModel.navigator = this
    }

    override fun onResume() {
        if (adsHandler != null) {
            adsHandler.postDelayed(adsRunnable, AppConst.TIMES_LOOP_PROMO);
        }
        super.onResume()
    }

    override fun onStop() {
        adsHandler.removeCallbacks(adsRunnable);
        super.onStop()
    }

    override fun onFragmentReady() {
        fragmentTopHeadlinesBinding = getViewDataBinding()

        topHeadlinesViewModel.fetchData()
        fragmentTopHeadlinesBinding?.let {
            it.swipe.setOnRefreshListener(this)
            topHeadlinesAdapter.setListener(this)

            it.recyclerView.itemAnimator = DefaultItemAnimator()
            it.recyclerView.layoutManager = (LinearLayoutManager(getBaseActivity()).apply { orientation = LinearLayoutManager.VERTICAL })
            it.recyclerView.adapter = topHeadlinesAdapter

            it.headlineViewPager.adapter = headlinePagerAdapter
            it.headlineViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(p0: Int) {
                }

                override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                }

                override fun onPageSelected(p0: Int) {
                    countPage = p0;
                }
            })
        }

        subscribeToLiveData()
    }

    private fun subscribeToLiveData() {
        topHeadlinesViewModel.getArticleListLiveData().observe(this, Observer {
            it?.let {
                topHeadlinesViewModel.addItemsToArticle(it)
            }
        })

        topHeadlinesViewModel.getArticleTopHeadlinesLiveData().observe(this, Observer {
            it?.let {
                adsHandler.removeCallbacks(adsRunnable);
                headlinePagerAdapter.setHeadlines(it)
                adsHandler.postDelayed(adsRunnable, AppConst.TIMES_LOOP_PROMO);
            }
        })
    }
    
    override fun onRefreshStop() {
        fragmentTopHeadlinesBinding?.let {
            if (it.swipe.isRefreshing) it.swipe.isRefreshing = false
        }
    }

    override fun onRefresh() {
        topHeadlinesViewModel.fetchEverything()
    }

    override fun onItemClick(articleUrl: String?, articleTitle: String?) {
        startActivity(getBaseActivity()?.DetailNewsIntent(articleUrl, articleTitle))
        getBaseActivity()?.overridePendingTransition(R.anim.slide_animation_enter, R.anim.nothing)
    }

    override fun onRetryClick() {
        topHeadlinesViewModel.fetchEverything()
    }

    override fun updateItemAdapter(position: Int, article: Article) {
        topHeadlinesAdapter.notifyItemChanged(position)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentDispatchingAndroidInjector

}