package com.cspro.newspedia.ui.main.topheadlines

import android.support.v7.widget.RecyclerView
import com.cspro.newspedia.databinding.ListItemEmptyBinding
import com.cspro.newspedia.databinding.ListItemTopHeadlinesBinding
import com.cspro.newspedia.ui.base.BaseViewHolder
import com.cspro.newspedia.app.AppLogger
import android.content.Intent
import android.databinding.ViewDataBinding
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cspro.newspedia.R
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.ui.base.BaseEmptyItemViewModel
import com.cspro.newspedia.ui.detail.DetailNewsIntent
import com.cspro.newspedia.ui.search.SearchIntent
import java.lang.RuntimeException


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class TopHeadlinesAdapter constructor(val articleListItems: MutableList<Article>) : RecyclerView.Adapter<BaseViewHolder>() {

    companion object {
        val VIEW_TYPE_EMPTY = 0
        val VIEW_TYPE_NORMAL = 1
    }

    private lateinit var listener: TopHeadlinesAdapterListener

    override fun getItemViewType(position: Int): Int {
        return if (articleListItems.isNotEmpty()) VIEW_TYPE_NORMAL
        else VIEW_TYPE_EMPTY
    }

    fun clearItems() {
        articleListItems.clear()
    }

    fun addItems(list: MutableList<Article>) {
        articleListItems.addAll(list)
        notifyDataSetChanged()
    }

    fun addItem(item: Article) {
        articleListItems.add(item)
        notifyDataSetChanged()
    }

    fun setListener(listener: TopHeadlinesAdapterListener) {
        this.listener = listener
    }

    override fun getItemCount(): Int = if (articleListItems.size > 0) articleListItems.size else 1

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder = when(viewType) {
        TopHeadlinesAdapter.VIEW_TYPE_NORMAL -> {
            val listItemTopHeadlinesBinding = ListItemTopHeadlinesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false)
            TopHeadlinesViewHolder(listItemTopHeadlinesBinding)
        }
        TopHeadlinesAdapter.VIEW_TYPE_EMPTY -> {
            val listItemEmptyBinding = ListItemEmptyBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false)
            EmptyViewHolder(listItemEmptyBinding)
        }
        else -> throw RuntimeException("The type is Empty and Normal")
    }

    inner class TopHeadlinesViewHolder(private val binding: ListItemTopHeadlinesBinding) : BaseViewHolder(binding.root),
        TopHeadlinesItemViewModel.TopHeadlinesItemViewModelListener{

        private lateinit var topHeadlinesItemViewModel: TopHeadlinesItemViewModel

        override fun onBind(position: Int) {
            val article = articleListItems[position]
            topHeadlinesItemViewModel = TopHeadlinesItemViewModel(article, this)
            binding.viewModel = topHeadlinesItemViewModel

            binding.executePendingBindings()
        }

        override fun onItemClick(articleUrl: String?, articleTitle: String?) {
            listener?.let { it.onItemClick(articleUrl, articleTitle) }
        }
    }

    inner class EmptyViewHolder(private val binding: ListItemEmptyBinding) : BaseViewHolder(binding.root),
        BaseEmptyItemViewModel.BaseEmptyItemViewModelListener {

        override fun onBind(position: Int) {
            val emptyItemViewModel = BaseEmptyItemViewModel(this)
            binding.setViewModel(emptyItemViewModel)
        }

        override fun onRetryClick() {
            listener?.let { it.onRetryClick() }
        }
    }

    interface TopHeadlinesAdapterListener {
        fun onRetryClick()

        fun onItemClick(articleUrl: String?, articleTitle: String?)
    }
}