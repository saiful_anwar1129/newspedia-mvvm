package com.cspro.newspedia.ui.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.DialogFragment
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import com.cspro.newspedia.utils.ConnectivityUtils
import com.cspro.newspedia.utils.DialogUtils
import dagger.android.AndroidInjection
import org.jetbrains.anko.toast

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel<*>> : AppCompatActivity(), BaseNavigator, BaseFragment.Callback {
    protected var progressDialog: DialogFragment? = null

    private var viewDataBinding: T? = null
    private var mViewModel: V? = null

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun getBindingVariable(): Int

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun getViewModel(): V

    abstract fun onActivityReady(savedInstanceState: Bundle?)

    fun getViewDataBinding(): T = viewDataBinding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
        performDataBinding()
        onActivityReady(savedInstanceState)
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun showLoading() {
        progressDialog = DialogUtils.newInstance()
        progressDialog?.show(this@BaseActivity.supportFragmentManager, DialogUtils.toString())
    }

    override fun showLoading(message: String) {
        progressDialog = DialogUtils.newInstance(message)
        progressDialog?.show(this@BaseActivity.supportFragmentManager, DialogUtils.toString())
    }

    override fun hideLoading() {
        runOnUiThread {
            progressDialog?.run {
                if (showsDialog)
                    dismiss()
                progressDialog = null
            }
        }
    }

    override fun onUnknownError(error: String?) {
        hideLoading()
        runOnUiThread { toast("onUnknownError: " + error) }
    }

    override fun onNetworkError() {
        hideLoading()
        toast("onNetworkError")
    }

    override fun onTimeout() {
        hideLoading()
        toast("onTimeout")
    }

    override fun isNetworkConnected(): Boolean = ConnectivityUtils.isConnectedToInternet(this)

    override fun onConnectionError() {
        toast("onConnectionError")
    }

    override fun onForbidden() {
        hideLoading()
        runOnUiThread {
            toast("onForbidden")
        }
    }

    override fun onUnauthorized() {
        hideLoading()
        runOnUiThread {
            toast("onUnauthorized")
        }
    }

    fun performDependencyInjection() = AndroidInjection.inject(this)

    private fun performDataBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutId())
        this.mViewModel = if (mViewModel == null) getViewModel() else mViewModel
        viewDataBinding?.let { it.setVariable(getBindingVariable(), mViewModel) }
        viewDataBinding?.let { it.executePendingBindings() }
    }
}