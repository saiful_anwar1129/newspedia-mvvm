package com.cspro.newspedia.ui.list

import android.arch.lifecycle.ViewModelProvider
import com.cspro.newspedia.app.ViewModelProviderFactory
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.ui.headline.HeadlinePagerAdapter
import com.cspro.newspedia.ui.main.topheadlines.TopHeadlinesAdapter
import com.cspro.newspedia.utils.SchedulerProvider
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
@Module
class ListNewsActivityModule{

    @Provides
    internal fun listNewsProvider(viewModel: ListNewsViewModel) : ViewModelProvider.Factory =
        ViewModelProviderFactory(viewModel)

    @Provides
    internal fun provideListNewsViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : ListNewsViewModel =
        ListNewsViewModel(dataManager, schedulerProvider, compositeDisposable)

    @Provides
    internal fun provideTopHeadlinesAdapter() : TopHeadlinesAdapter = TopHeadlinesAdapter(ArrayList())

    @Provides
    fun provideHeadlinePagerAdapter(gson: Gson, listNewsActivity: ListNewsActivity) : HeadlinePagerAdapter =
        HeadlinePagerAdapter(ArrayList(), gson, listNewsActivity.supportFragmentManager)
}