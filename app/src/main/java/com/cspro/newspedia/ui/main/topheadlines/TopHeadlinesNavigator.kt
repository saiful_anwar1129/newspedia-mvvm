package com.cspro.newspedia.ui.main.topheadlines

import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.ui.base.BaseNavigator


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
interface TopHeadlinesNavigator : BaseNavigator {

    fun onRefreshStop()

    fun updateItemAdapter(position: Int, article: Article)

}