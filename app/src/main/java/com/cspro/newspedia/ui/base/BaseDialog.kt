package com.cspro.newspedia.ui.base

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import com.cspro.newspedia.utils.ConnectivityUtils
import org.jetbrains.anko.support.v4.toast

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
abstract class BaseDialog : DialogFragment(), BaseNavigator {

    private var baseActivity: BaseActivity<*, *>? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            val activity = context as BaseActivity<*, *>?
            this.baseActivity = activity
            activity?.onFragmentAttached()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val root = RelativeLayout(activity)
        root.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)

        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(root)
        dialog.window?.let {
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            it.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
        }
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    override fun show(manager: FragmentManager?, tag: String?) {
        manager?.let {
            val transaction = it.beginTransaction()
            val prevFrag = it.findFragmentByTag(tag)
            prevFrag?.let {
                transaction.remove(it)
            }
            transaction.addToBackStack(null)
            show(transaction, tag)
        }
    }

    open fun dismissDialog(tag: String) {
        dismiss()
        baseActivity?.let { it.onFragmentDetached(tag) }
    }

    override fun showLoading() {
        toast("showLoading")
    }

    override fun showLoading(message: String) {
        toast("hideLoading")
    }

    override fun hideLoading() {
        toast("hideLoading")
    }

    override fun onUnknownError(error: String?) {
        hideLoading()
        baseActivity?.runOnUiThread { toast("onUnknownError: " + error) }
    }

    override fun onNetworkError() {
        hideLoading()
        toast("onNetworkError")
    }

    override fun onTimeout() {
        hideLoading()
        toast("onTimeout")
    }

    override fun onConnectionError() {
        toast("onConnectionError")
    }

    override fun onForbidden() {
        hideLoading()
        baseActivity?.runOnUiThread {
            toast("onForbidden")
        }
    }

    override fun onUnauthorized() {
        hideLoading()
        baseActivity?.runOnUiThread {
            toast("onUnauthorized")
        }
    }

    override fun isNetworkConnected(): Boolean = baseActivity != null && ConnectivityUtils.isConnectedToInternet(this.context)

    fun hideKeyboard() {
        baseActivity?.let {
            val view = it.currentFocus
            if (view != null) {
                val imm = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }

    fun getBaseActivity() = baseActivity
}