package com.cspro.newspedia.ui.main.browse

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.cspro.newspedia.BR
import com.cspro.newspedia.R
import com.cspro.newspedia.ui.base.BaseFragment
import com.cspro.newspedia.databinding.FragmentBrowseBinding
import com.cspro.newspedia.ui.detail.DetailNewsIntent
import com.cspro.newspedia.ui.list.ListNewsIntent
import javax.inject.Inject

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class BrowseFragment : BaseFragment<FragmentBrowseBinding, BrowseViewModel>(), BrowseNavigator,
    SwipeRefreshLayout.OnRefreshListener, SourceAdapter.SourceAdapterListener {

    companion object {
        val TAG = BrowseFragment::class.java.simpleName

        fun newInstance() : BrowseFragment = BrowseFragment()
    }

    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var sourceAdater: SourceAdapter

    private var fragmentBrowseBinding: FragmentBrowseBinding? = null
    internal lateinit var browseViewModel: BrowseViewModel

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.fragment_browse

    override fun getViewModel(): BrowseViewModel {
        browseViewModel = ViewModelProviders.of(this, mViewModelFactory).get(BrowseViewModel::class.java)
        return browseViewModel
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        browseViewModel.navigator = this
    }

    override fun onFragmentReady() {
        fragmentBrowseBinding = getViewDataBinding()

        browseViewModel.fetchSourceList()
        fragmentBrowseBinding?.let {
            it.swipe.setOnRefreshListener(this)
            sourceAdater.setListener(this)

            it.recyclerView.itemAnimator = DefaultItemAnimator()
            it.recyclerView.layoutManager = (LinearLayoutManager(getBaseActivity()).apply { orientation = LinearLayoutManager.VERTICAL })
            it.recyclerView.adapter = sourceAdater
        }

        subscribeToLiveData()
    }

    private fun subscribeToLiveData() {
        browseViewModel.getSourceListLiveData().observe(this, Observer {
            it?.let {
                browseViewModel.addItemsToSource(it)
            }
        })
    }

    override fun onRefreshStop() {
        fragmentBrowseBinding?.let {
            if (it.swipe.isRefreshing) it.swipe.isRefreshing = false
        }
    }

    override fun onRefresh() {
        browseViewModel.fetchSourceList()
    }

    override fun onItemClick(sourceId: String?, name: String?) {
        startActivity(getBaseActivity()?.ListNewsIntent(sourceId, name))
        getBaseActivity()?.overridePendingTransition(R.anim.slide_animation_enter, R.anim.nothing)
    }

    override fun onRetryClick() {
        browseViewModel.fetchSourceList()
    }
}