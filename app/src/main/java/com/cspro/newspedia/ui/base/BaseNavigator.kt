package com.cspro.newspedia.ui.base

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
interface BaseNavigator {

    fun showLoading(message: String)

    fun showLoading()

    fun hideLoading()

    fun onUnknownError(error: String?)

    fun onTimeout()

    fun onNetworkError()

    fun isNetworkConnected(): Boolean

    fun onConnectionError()

    fun onUnauthorized()

    fun onForbidden()
}