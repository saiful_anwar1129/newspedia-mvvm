package com.cspro.newspedia.ui.headline

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.cspro.newspedia.data.network.models.Article
import com.google.gson.Gson


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class HeadlinePagerAdapter constructor(articleListItems: MutableList<Article>, private val gson: Gson,
                                       fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    private var articleListItems: MutableList<Article> = articleListItems

    fun setHeadlines(articles: List<Article>) {
        this.articleListItems.clear()
        this.articleListItems.addAll(articles)
        notifyDataSetChanged()
    }

    override fun getCount(): Int = this.articleListItems.size

    override fun getItem(position: Int): Fragment? = HeadlineFragment.newInstance(articleGson = gson.toJson(this.articleListItems[position]))
}