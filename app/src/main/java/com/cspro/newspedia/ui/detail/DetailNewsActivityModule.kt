package com.cspro.newspedia.ui.detail

import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.ui.splash.SplashViewModel
import com.cspro.newspedia.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
@Module
class DetailNewsActivityModule {

    @Provides
    internal fun provideDetailNewsViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : DetailNewsViewModel
            = DetailNewsViewModel(dataManager, schedulerProvider, compositeDisposable)

}