package com.cspro.newspedia.ui.main

import android.support.v4.app.Fragment
import com.cspro.newspedia.R
import com.cspro.newspedia.ui.main.browse.BrowseFragment
import com.cspro.newspedia.ui.main.setting.SettingFragment
import com.cspro.newspedia.ui.main.topheadlines.TopHeadlinesFragment

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
enum class BottomNavigationPosition(val position: Int, val id: Int) {
    TOP_HEADLINES(0, R.id.menuTopHeadLine),
    BROWSE(1, R.id.menuBrowse),
    SETTING(2, R.id.menuSetting)
}

fun findNavigationPositionById(id: Int): BottomNavigationPosition = when (id) {
    BottomNavigationPosition.TOP_HEADLINES.id -> BottomNavigationPosition.TOP_HEADLINES
    BottomNavigationPosition.BROWSE.id -> BottomNavigationPosition.BROWSE
    BottomNavigationPosition.SETTING.id -> BottomNavigationPosition.SETTING
    else -> BottomNavigationPosition.TOP_HEADLINES
}

fun BottomNavigationPosition.createFragment(): Fragment = when (this) {
    BottomNavigationPosition.TOP_HEADLINES -> TopHeadlinesFragment.newInstance()
    BottomNavigationPosition.BROWSE -> BrowseFragment.newInstance()
    BottomNavigationPosition.SETTING -> SettingFragment.newInstance()
}

fun BottomNavigationPosition.getTag(): String = when (this) {
    BottomNavigationPosition.TOP_HEADLINES -> TopHeadlinesFragment.TAG
    BottomNavigationPosition.BROWSE -> BrowseFragment.TAG
    BottomNavigationPosition.SETTING -> SettingFragment.TAG
}
