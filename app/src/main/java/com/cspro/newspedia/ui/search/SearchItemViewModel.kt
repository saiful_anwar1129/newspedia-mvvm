package com.cspro.newspedia.ui.search

import android.databinding.ObservableField
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.utils.formatDate


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class SearchItemViewModel constructor(article: Article, private val listener: SearchItemViewModelListener) {
    private val article: Article = article

    val title: ObservableField<String>
    val publishAt: ObservableField<String>
    val coverImage: ObservableField<String>

    init {
        this.title = ObservableField(this.article.title)
        this.publishAt = ObservableField(formatDate(this.article.publishedAt))
        this.coverImage = ObservableField(this.article?.urlToImage ?: "")
    }

    fun onItemClick() = listener.onItemClick(this.article.url, this.article.title)

    interface SearchItemViewModelListener {
        fun onItemClick(articleUrl: String?, articleTitle: String?)
    }

}