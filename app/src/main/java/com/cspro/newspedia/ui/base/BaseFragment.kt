package com.cspro.newspedia.ui.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cspro.newspedia.utils.ConnectivityUtils
import com.cspro.newspedia.utils.DialogUtils
import dagger.android.support.AndroidSupportInjection
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.toast

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel<*>> : Fragment(), BaseNavigator {

    private var baseActivity: BaseActivity<*, *>? = null
    private var mRootView: View? = null
    private var viewDataBinding: T? = null
    private var mViewModel: V? = null
    protected var progressDialog: DialogFragment? = null

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun getBindingVariable(): Int

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): V

    abstract fun onFragmentReady()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            val activity = context as BaseActivity<*, *>?
            this.baseActivity = activity
            activity?.onFragmentAttached()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
        mViewModel = getViewModel()
        setHasOptionsMenu(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        mRootView = viewDataBinding?.root
        return mRootView
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding?.setVariable(getBindingVariable(), mViewModel)
        viewDataBinding?.executePendingBindings()

        onFragmentReady()
    }

    override fun showLoading() {
        progressDialog = DialogUtils.newInstance()
        progressDialog?.show(baseActivity?.supportFragmentManager, DialogUtils.toString())
    }

    override fun showLoading(message: String) {
        progressDialog = DialogUtils.newInstance(message)
        progressDialog?.show(baseActivity?.supportFragmentManager, DialogUtils.toString())
    }

    override fun hideLoading() {
        baseActivity?.runOnUiThread {
            progressDialog?.run {
                if (showsDialog)
                    dismiss()
                progressDialog = null
            }
        }
    }

    override fun onUnknownError(error: String?) {
        hideLoading()
        baseActivity?.runOnUiThread { toast("onUnknownError: " + error) }
    }

    override fun onNetworkError() {
        hideLoading()
        toast("onNetworkError")
    }

    override fun onTimeout() {
        hideLoading()
        toast("onTimeout")
    }
    
    override fun onConnectionError() {
        toast("onConnectionError")
    }

    override fun onForbidden() {
        hideLoading()
        baseActivity?.runOnUiThread {
            toast("onForbidden")
        }
    }

    override fun onUnauthorized() {
        hideLoading()
        baseActivity?.runOnUiThread {
            toast("onUnauthorized")
        }
    }

    override fun isNetworkConnected(): Boolean = baseActivity != null && ConnectivityUtils.isConnectedToInternet(this.context)

    private fun performDependencyInjection() {
        AndroidSupportInjection.inject(this)
    }

    fun getBaseActivity() = baseActivity

    fun getViewDataBinding() = viewDataBinding

    interface Callback {

        fun onFragmentAttached()

        fun onFragmentDetached(tag: String)
    }
}