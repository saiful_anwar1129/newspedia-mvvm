package com.cspro.newspedia.ui.search

import android.arch.lifecycle.MutableLiveData
import android.support.v7.widget.SearchView
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.utils.SchedulerProvider
import com.cspro.newspedia.utils.fromView
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import com.cspro.newspedia.app.AppLogger
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.utils.diffPublishedHourMinutes
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class SearchNewsViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<SearchNewsNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable) {
    private val articleList: ObservableList<Article>
    private val articleListLiveData: MutableLiveData<List<Article>>

    init {
        this.articleList = ObservableArrayList<Article>()
        this.articleListLiveData = MutableLiveData()
    }

    fun performSearhView(searchView: SearchView, sourceId: String) {
        fromView(searchView)
            .map { text -> text.toLowerCase().trim() }
            .debounce(300, TimeUnit.MILLISECONDS)
            .filter { text -> text.isNotBlank() }
            .distinctUntilChanged()
            .toFlowable(BackpressureStrategy.BUFFER)
            .switchMap { query ->
                setIsLoading(true)
                dataManager.getEverything(query = query, sourceId = if(sourceId.isNotBlank()) sourceId else null)
                    .observeOn(schedulerProvider.getMainThreadScheduler())
                    .subscribeOn(schedulerProvider.getIOThreadScheduler())
            }
            .compose(schedulerProvider.ioToMainFlowableScheduler())
            .subscribe(
                {
                    setIsLoading(false)
                    if (it.isSuccessful())
                        it.data?.let { articleListLiveData.value = it }
                    else navigator.onUnknownError(it.message)
                },
                {
                    setIsLoading(false)
                    AppLogger.d("Search error: ${it.message}")
                    navigator.onUnknownError(it.message)
                }
            )
    }

    fun addItemsToArticle(list: List<Article>) {
        articleList.clear()
        articleList.addAll(list)

    }

    fun getArticleList() : ObservableList<Article> = articleList

    fun getArticleListLiveData() : MutableLiveData<List<Article>> = articleListLiveData

}
