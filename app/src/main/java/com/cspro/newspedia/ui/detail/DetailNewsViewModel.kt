package com.cspro.newspedia.ui.detail

import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class DetailNewsViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<DetailNewsNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable)
