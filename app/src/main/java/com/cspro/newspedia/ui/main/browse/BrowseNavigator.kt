package com.cspro.newspedia.ui.main.browse

import com.cspro.newspedia.ui.base.BaseNavigator


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
interface BrowseNavigator : BaseNavigator {

    fun onRefreshStop()

}