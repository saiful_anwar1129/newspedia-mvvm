package com.cspro.newspedia.ui.list

import com.cspro.newspedia.ui.base.BaseNavigator


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
interface ListNewsNavigator : BaseNavigator {

    fun onRefreshStop()

    fun openSearchActivity()

}