package com.cspro.newspedia.ui.main.setting

import com.cspro.newspedia.ui.base.BaseNavigator
import com.jakewharton.rxbinding2.InitialValueObservable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/29/2019.
 */
interface SettingNavigator : BaseNavigator {

    fun successUpdate(message: String)
}