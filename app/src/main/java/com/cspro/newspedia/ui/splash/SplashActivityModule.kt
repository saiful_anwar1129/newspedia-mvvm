package com.cspro.newspedia.ui.splash

import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
@Module
class SplashActivityModule {

    @Provides
    internal fun provideSplashViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : SplashViewModel
            = SplashViewModel(dataManager, schedulerProvider, compositeDisposable)

}