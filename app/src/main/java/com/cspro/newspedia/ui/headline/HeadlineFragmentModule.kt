package com.cspro.newspedia.ui.headline

import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.ui.main.topheadlines.TopHeadlinesFragment
import com.cspro.newspedia.utils.SchedulerProvider
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
@Module
class HeadlineFragmentModule{

    @Provides
    internal fun provideHeadlineViewModel(gson: Gson, dataManager: DataManager, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : HeadlineViewModel =
        HeadlineViewModel(gson, dataManager, schedulerProvider, compositeDisposable)
}