package com.cspro.newspedia. ui.main.topheadlines

import android.databinding.ObservableField
import com.cspro.newspedia.data.network.models.Article


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class TopHeadlinesItemViewModel constructor(article: Article, private val listener: TopHeadlinesItemViewModelListener) {
    private val article: Article = article

    val title: ObservableField<String>
    val publishAt: ObservableField<String>
    val sourceName: ObservableField<String>
    val coverImage: ObservableField<String>

    init {
        this.title = ObservableField(this.article.title)
        this.publishAt = ObservableField(this.article.publishedAt)
        this.sourceName = ObservableField(this.article.source?.name ?: "")
        this.coverImage = ObservableField(this.article?.urlToImage ?: "")
    }

    fun onItemClick() = listener.onItemClick(this.article.url, this.article.title)

    interface TopHeadlinesItemViewModelListener {
        fun onItemClick(articleUrl: String?, articleTitle: String?)
    }

}