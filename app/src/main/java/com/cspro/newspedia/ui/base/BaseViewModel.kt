package com.cspro.newspedia.ui.base

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.data.model.api.response.ListSourcesResponse
import com.cspro.newspedia.data.model.db.Country
import com.cspro.newspedia.utils.SchedulerProvider
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.lang.RuntimeException
import java.lang.ref.WeakReference

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
abstract class BaseViewModel<N>(val dataManager: DataManager, val compositeDisposable: CompositeDisposable, val schedulerProvider: SchedulerProvider) : ViewModel() {

    val isLoading = ObservableBoolean(false)

    private lateinit var mNavigator: WeakReference<N>

    var navigator: N
        get() = mNavigator!!.get()!!
        set(navigator) {
            this.mNavigator = WeakReference(navigator)
        }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    fun setIsLoading(status: Boolean) = isLoading.set(status)

    fun getSources(countryNow: Country) : Observable<Int> = dataManager.getSources(countryNow.code.toLowerCase())
    .doOnNext { if (!it.isSuccessful()) throw RuntimeException("Data sources not found") }
    .takeUntil { it.isSuccessful() }
    .concatMapMaybe { response ->
        response.data?.let { sourceList ->
            dataManager.updateCountry(countryNow.apply {
                sources = sourceList.joinToString(separator = ",") { it.id }
            }).flatMap { dataManager.updateToDefaultCountry(countryNow.id) }
        }
    }

}