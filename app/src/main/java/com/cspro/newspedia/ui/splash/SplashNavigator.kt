package com.cspro.newspedia.ui.splash

import com.cspro.newspedia.ui.base.BaseNavigator

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
interface SplashNavigator : BaseNavigator {

    fun openMainActivity()

}