package com.cspro.newspedia.ui.detail

import android.os.Bundle
import com.cspro.newspedia.BR
import com.cspro.newspedia.databinding.ActivityDetailNewsBinding
import com.cspro.newspedia.ui.base.BaseActivity
import javax.inject.Inject
import com.cspro.newspedia.R
import android.support.v7.app.AlertDialog
import android.view.View.GONE
import android.webkit.WebView
import android.webkit.WebChromeClient
import android.net.http.SslError
import android.webkit.SslErrorHandler
import android.webkit.WebViewClient
import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.view.View


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class DetailNewsActivity : BaseActivity<ActivityDetailNewsBinding, DetailNewsViewModel>(), DetailNewsNavigator {

    @Inject
    internal lateinit var detailNewsViewModel: DetailNewsViewModel

    private lateinit var activityDetailNewsBinding: ActivityDetailNewsBinding
    private lateinit var title: String
    private lateinit var url: String

    override fun getLayoutId(): Int = R.layout.activity_detail_news

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getViewModel(): DetailNewsViewModel = detailNewsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityReady(savedInstanceState: Bundle?) {
        title = intent.getStringExtra(PARAM_DETAIL_NEWS_TITLE)
        url = intent.getStringExtra(PARAM_DETAIL_NEWS_URL)
        requireNotNull(title) { "title provided in Intent extras" }
        requireNotNull(url) { "url provided in Intent extras" }

        detailNewsViewModel.navigator = this
        activityDetailNewsBinding = getViewDataBinding()

        activityDetailNewsBinding?.let {
            setSupportActionBar(it.toolbar)
            it.toolbar.setTitle(title)
            supportActionBar?.also {
                it.setDisplayHomeAsUpEnabled(true)
                it.setDisplayShowHomeEnabled(true)
            }
            it.toolbar.setNavigationOnClickListener { onBackPressed() }

            if (isNetworkConnected()) {
                it.wb.loadUrl(url);
                it.wb.getSettings().setJavaScriptEnabled(true)
                it.wb.getSettings().setAllowFileAccessFromFileURLs(true)

                it.wb.setWebViewClient(object : WebViewClient() {
                    override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                        val builder = AlertDialog.Builder(this@DetailNewsActivity)
                        builder.setMessage(R.string.notification_error_ssl_cert_invalid)
                        builder.setPositiveButton(
                            resources.getString(R.string.next)
                        ) { _, _ -> handler.proceed() }
                        builder.setNegativeButton(
                            resources.getString(R.string.cancel)
                        ) { _, _ -> handler.cancel() }
                        val dialog = builder.create()
                        dialog.show()
                    }
                })

                it.wb.setWebChromeClient(object : WebChromeClient() {
                    override fun onProgressChanged(view: WebView, progress: Int) {
                        it.progressBar.setProgress(progress)
                        if (progress == 100) {
                            it.progressBar.setVisibility(View.GONE)
                        }
                    }
                })
            } else {
                AlertDialog.Builder(this@DetailNewsActivity)
                    .setTitle(resources.getString(R.string.some_error))
                    .setMessage(resources.getString(R.string.connection_error))
                    .setPositiveButton(resources.getString(R.string.ok), { _, _ ->
                        val intent = Intent(Settings.ACTION_WIRELESS_SETTINGS)
                        startActivity(intent)
                    }).show()
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.nothing, R.anim.slide_animation_leave)
    }
}

val PARAM_DETAIL_NEWS_URL = "PARAM_DETAIL_NEWS_URL"
val PARAM_DETAIL_NEWS_TITLE = "PARAM_DETAIL_NEWS_TITLE"
fun Context.DetailNewsIntent(url: String?, title: String?) : Intent = Intent(this, DetailNewsActivity::class.java).apply {
    putExtra(PARAM_DETAIL_NEWS_URL, url)
    putExtra(PARAM_DETAIL_NEWS_TITLE, title)
}