package com.cspro.newspedia.ui.headline

import android.databinding.ObservableField
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.ui.base.BaseNavigator
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.ui.splash.SplashNavigator
import com.cspro.newspedia.utils.SchedulerProvider
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class HeadlineViewModel @Inject constructor(val gson: Gson, dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<HeadlineNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable) {
    private lateinit var article: Article

    val title: ObservableField<String>
    val publishAt: ObservableField<String>
    val sourceName: ObservableField<String>
    val coverImage: ObservableField<String>

    init {
        this.title = ObservableField()
        this.publishAt = ObservableField()
        this.sourceName = ObservableField()
        this.coverImage = ObservableField()
    }

    fun fetchArticle(articleGson: String?) {
        articleGson?.let {
            try {
                article = gson.fromJson<Article>(it, Article::class.java)
                this.title.set(article.title)
                this.publishAt.set(article.publishedAt)
                this.sourceName.set(article.source?.name ?: "")
                this.coverImage.set(article?.urlToImage ?: "")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun onItemClick() = article?.let { navigator.openDetailNewsActivity(it.url, it.title) }

}