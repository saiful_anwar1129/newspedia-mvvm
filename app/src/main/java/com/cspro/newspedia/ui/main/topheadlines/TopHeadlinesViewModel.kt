package com.cspro.newspedia.ui.main.topheadlines

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import android.databinding.ObservableList
import android.os.Build
import android.support.annotation.RequiresApi
import com.cspro.newspedia.app.AppConst
import com.cspro.newspedia.app.AppLogger
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.data.model.api.response.ListArticlesResponse
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.ui.base.BaseNavigator
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.utils.CallbackWrapper
import com.cspro.newspedia.utils.SchedulerProvider
import com.cspro.newspedia.utils.diffPublishedHour
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import java.util.stream.Collectors
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class TopHeadlinesViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<TopHeadlinesNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable) {
    private val articleList: ObservableList<Article>
    private val articleListLiveData: MutableLiveData<List<Article>>
    private val articleTopHeadlines: ObservableList<Article>
    private val articleTopHeadlinesLiveData: MutableLiveData<List<Article>>
    val countryName: ObservableField<String>

    init {
        this.articleList = ObservableArrayList<Article>()
        this.articleListLiveData = MutableLiveData()
        this.articleTopHeadlines = ObservableArrayList<Article>()
        this.articleTopHeadlinesLiveData = MutableLiveData()
        this.countryName = ObservableField("")
    }

    fun fetchData() {
        if (navigator.isNetworkConnected()) {
            setIsLoading(true)

            compositeDisposable.add(dataManager.loadCountriesIsDefault()
                .toFlowable(BackpressureStrategy.BUFFER)
                .flatMap {
                    if (it.isNotEmpty()) {
                        it.first().run {
                            if (sources != null && sources!!.isNotBlank()) {
                                sources?.let {
                                    countryName.set("Most Popularity in " + this.name)
                                    zipFetch(it, "popularity")
                                }
                            } else throw RuntimeException("Sources not found")
                        }
                    } else throw RuntimeException("Data country default empty")
                }
                .compose(schedulerProvider.ioToMainFlowableScheduler())
                .doOnTerminate { setIsLoading(false) }
                .subscribe(
                    {
                        articleListLiveData.value = it.first
                        articleTopHeadlinesLiveData.value = it.second
                    },
                    { err -> navigator.onUnknownError(err.message) }
                ))
        } else navigator.onConnectionError()
    }

    fun fetchEverything() {
        if (navigator.isNetworkConnected()) {
            setIsLoading(true)

            compositeDisposable.add(dataManager.loadCountriesIsDefault()
                .toFlowable(BackpressureStrategy.BUFFER)
                .flatMap {
                    if (it.isNotEmpty()) {
                        it.first().run {
                            if (sources != null && sources!!.isNotBlank()) {
                                sources?.let {
                                    countryName.set("Most Popularity in " + this.name)
                                    everythingFlowable(it, "popularity")
                                }
                            } else throw RuntimeException("Sources not found")
                        }
                    } else throw RuntimeException("Data country default empty")
                }
                .compose(schedulerProvider.ioToMainFlowableScheduler())
                .doOnTerminate {
                    navigator.onRefreshStop()
                    setIsLoading(false)
                }
                .subscribe(
                    { articleListLiveData.value = it },
                    {
                        articleList.clear()
                        navigator.onUnknownError(it.message)
                    }
                ))
        } else navigator.onConnectionError()
    }

    private fun zipFetch(sources: String, sortBy: String) : Flowable<Pair<List<Article>, List<Article>>> =
        Flowable.zip(everythingFlowable(sources, sortBy)
            .observeOn(schedulerProvider.getMainThreadScheduler()),
            topHeadlinesFlowable()
                .observeOn(schedulerProvider.getMainThreadScheduler()),
            BiFunction { t1, t2 -> Pair(t1, t2) })


    private fun everythingFlowable(sources: String, sortBy: String) : Flowable<List<Article>> =
        dataManager.getEverything(sourceId = sources, sortBy = sortBy)
            .subscribeOn(schedulerProvider.getIOThreadScheduler())
            .doOnNext { if (!it.isSuccessful()) RuntimeException(it.message) }
            .flatMap { Flowable.fromIterable(it.data) }
            .map { article -> article.apply {
                publishedAt = diffPublishedHour(article.publishedAt)
            } }
            .toList()
            .toFlowable()


    private fun topHeadlinesFlowable() : Flowable<List<Article>> =
        dataManager.getTopHeadlines(country = "us")
            .subscribeOn(schedulerProvider.getIOThreadScheduler())
            .doOnNext { if (!it.isSuccessful()) RuntimeException(it.message) }
            .flatMap { Flowable.fromIterable(it.data) }
            .take(AppConst.MAX_LIMIT_PROMO)
            .map { article -> article.apply {
                publishedAt = diffPublishedHour(article.publishedAt, " jam yang lalu")
            } }
            .toList()
            .toFlowable()

    fun addItemsToArticle(list: List<Article>) {
        articleList.clear()
        articleList.addAll(list)
    }

    fun addItemsToArticleTopHeadlines(list: List<Article>) {
        articleTopHeadlines.clear()
        articleTopHeadlines.addAll(list)
    }

    fun getArticleList() : ObservableList<Article> = articleList

    fun getArticleListLiveData() : MutableLiveData<List<Article>> = articleListLiveData

    fun getArticleTopHeadlinesLiveData() : MutableLiveData<List<Article>> = articleTopHeadlinesLiveData
}
