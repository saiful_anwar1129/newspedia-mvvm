package com.cspro.newspedia.ui.main.setting

import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Module
abstract class SettingFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(SettingFragmentModule::class))
    abstract fun provideSettingFragmentFactory() : SettingFragment

}