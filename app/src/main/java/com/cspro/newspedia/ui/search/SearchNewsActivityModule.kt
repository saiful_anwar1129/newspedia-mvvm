package com.cspro.newspedia.ui.search

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import com.cspro.newspedia.app.ViewModelProviderFactory
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Module
class SearchNewsActivityModule{

    @Provides
    internal fun topHeadlinesProvider(newsViewModel: SearchNewsViewModel) : ViewModelProvider.Factory =
        ViewModelProviderFactory(newsViewModel)

    @Provides
    internal fun provideSearchViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : SearchNewsViewModel =
        SearchNewsViewModel(dataManager, schedulerProvider, compositeDisposable)

    @Provides
    internal fun provideSearchNewsAdapter() : SearchNewsAdapter = SearchNewsAdapter(ArrayList())

    @Provides
    internal fun provideLayoutManager(activity: SearchNewsActivity) : LinearLayoutManager = LinearLayoutManager(activity)

}