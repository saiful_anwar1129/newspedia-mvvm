package com.cspro.newspedia.ui.headline

import dagger.Module
import dagger.android.ContributesAndroidInjector


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
@Module
abstract class HeadlineFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(HeadlineFragmentModule::class))
    abstract fun provideHeadlineFragmentFactory() : HeadlineFragment

}