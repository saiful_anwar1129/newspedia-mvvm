package com.cspro.newspedia.ui.main.setting

import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Module
class SettingFragmentModule{

    @Provides
    internal fun provideSettingViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : SettingViewModel =
        SettingViewModel(dataManager, schedulerProvider, compositeDisposable)

}