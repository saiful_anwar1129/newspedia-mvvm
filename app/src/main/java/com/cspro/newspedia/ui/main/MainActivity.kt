package com.cspro.newspedia.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.Menu
import android.view.MenuItem
import com.cspro.newspedia.ui.base.BaseActivity
import dagger.android.support.HasSupportFragmentInjector
import com.cspro.newspedia.R
import com.cspro.newspedia.BR
import com.cspro.newspedia.databinding.ActivityMainBinding
import com.cspro.newspedia.utils.extension.removeFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import com.cspro.newspedia.ui.search.SearchIntent
import org.jetbrains.anko.toast


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>(), MainNavigator, BottomNavigationView.OnNavigationItemSelectedListener, HasSupportFragmentInjector {

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    internal lateinit var mainViewModel: MainViewModel

    private lateinit var activityMainBinding: ActivityMainBinding
    private val KEY_POSITION = "KEY_POSITION"
    private var navPosition: BottomNavigationPosition = BottomNavigationPosition.TOP_HEADLINES
    private var doubleBackToExitPressedOnce: Boolean = false;
    private var optionMenu: Menu? = null

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun getViewModel(): MainViewModel = mainViewModel

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putInt(KEY_POSITION, navPosition.id)
        super.onSaveInstanceState(outState)
    }

    private fun restoreSaveInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.also {
            val id = it.getInt(KEY_POSITION, BottomNavigationPosition.TOP_HEADLINES.id)
            navPosition = findNavigationPositionById(id)
        }
    }

    override fun onActivityReady(savedInstanceState: Bundle?) {
        activityMainBinding = getViewDataBinding()
        mainViewModel.navigator = this

        activityMainBinding?.let {
            setSupportActionBar(it.toolbar)
        }
        initBottomNavigation()
        initFragment(savedInstanceState)
    }

    private fun initBottomNavigation() {
        navigation.disableShiftMode()
        navigation.active(navPosition.position)

        navigation.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        navPosition = findNavigationPositionById(item.itemId)
        optionMenu?.let { updateOptionMenu(item) }
        return switchFragment(navPosition)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        optionMenu = menu
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when(it.itemId) {
                R.id.searchMenu -> {
                    openSearchActivity()
                    return true
                }
                else -> {
                    return super.onOptionsItemSelected(item)
                }
            }
        }
    }

    override fun openSearchActivity() {
        startActivity(SearchIntent())
        overridePendingTransition(R.anim.slide_animation_enter, R.anim.nothing)
    }

    private fun initFragment(savedInstanceState: Bundle?) {
        savedInstanceState ?: switchFragment(BottomNavigationPosition.TOP_HEADLINES)
    }

    private fun switchFragment(navPosition: BottomNavigationPosition): Boolean {
        val fragment = supportFragmentManager.findFragment(navPosition)
        if (fragment.isAdded) return false
        detachFragment()
        attachFragment(fragment, navPosition.getTag())
        supportFragmentManager.executePendingTransactions()
        return true
    }

    private fun FragmentManager.findFragment(position: BottomNavigationPosition): Fragment {
        return findFragmentByTag(position.getTag()) ?: position.createFragment()
    }

    private fun detachFragment() {
        supportFragmentManager.findFragmentById(R.id.container)?.also {
            supportFragmentManager.beginTransaction().detach(it).commit()
        }
    }

    private fun attachFragment(fragment: Fragment, tag: String) {
        if (fragment.isDetached) {
            supportFragmentManager.beginTransaction().attach(fragment).commit()
        } else {
            supportFragmentManager.beginTransaction().add(R.id.container, fragment, tag).commit()
        }
        supportFragmentManager.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .commit()
    }

    override fun onFragmentAttached() {
        super.onFragmentAttached()
    }

    override fun onFragmentDetached(tag: String) {
        supportFragmentManager?.removeFragment(tag = tag)
    }

    private fun updateOptionMenu(item: MenuItem) {
        optionMenu?.let {
            if (item.itemId == R.id.menuTopHeadLine) it.findItem(R.id.searchMenu).setVisible(true)
            else it.findItem(R.id.searchMenu).setVisible(false)

            onPrepareOptionsMenu(optionMenu)
        }
    }

    private fun twiceOnClick() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity()
        }
        doubleBackToExitPressedOnce = true
        toast(getString(R.string.twice_click_to_exit))
        Handler().postDelayed({
            doubleBackToExitPressedOnce = false;
        }, 2000)
    }

    override fun onBackPressed() {
        twiceOnClick()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentDispatchingAndroidInjector
}

fun Context.MainIntent() : Intent = Intent(this, MainActivity::class.java)