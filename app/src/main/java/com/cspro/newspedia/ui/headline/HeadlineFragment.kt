package com.cspro.newspedia.ui.headline

import android.os.Bundle
import com.cspro.newspedia.BR
import com.cspro.newspedia.R
import com.cspro.newspedia.databinding.FragmentHeadlineBinding
import com.cspro.newspedia.ui.base.BaseFragment
import com.cspro.newspedia.ui.detail.DetailNewsIntent
import com.cspro.newspedia.utils.withArgs
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class HeadlineFragment : BaseFragment<FragmentHeadlineBinding, HeadlineViewModel>(), HeadlineNavigator {

    companion object {
        val TAG = HeadlineFragment::class.java.simpleName
        private val PARAM_ARTICLE_GSON = "PARAM_ARTICLE_GSON"

        fun newInstance(articleGson: String) : HeadlineFragment = HeadlineFragment().withArgs {
            putString(PARAM_ARTICLE_GSON, articleGson)
        }
    }

    @Inject
    internal lateinit var headlineViewModel: HeadlineViewModel

    private var articleGson: String? = null

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.fragment_headline

    override fun getViewModel(): HeadlineViewModel = headlineViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        headlineViewModel.navigator = this
        articleGson = arguments?.getString(PARAM_ARTICLE_GSON)
        requireNotNull(articleGson) { "articleGson provided in Intent extras" }
    }

    override fun onFragmentReady() {
        headlineViewModel.fetchArticle(articleGson)
    }

    override fun openDetailNewsActivity(articleUrl: String?, articleTitle: String?) {
        startActivity(getBaseActivity()?.DetailNewsIntent(articleUrl, articleTitle))
        getBaseActivity()?.overridePendingTransition(R.anim.slide_animation_enter, R.anim.nothing)
    }
}