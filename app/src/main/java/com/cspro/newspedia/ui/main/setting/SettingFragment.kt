package com.cspro.newspedia.ui.main.setting

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import com.cspro.newspedia.BR
import com.cspro.newspedia.R
import com.cspro.newspedia.ui.base.BaseFragment
import com.cspro.newspedia.databinding.FragmentSettingBinding
import org.jetbrains.anko.support.v4.toast
import javax.inject.Inject
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.view.View
import android.view.ViewGroup
import com.cspro.newspedia.app.AppLogger


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class SettingFragment : BaseFragment<FragmentSettingBinding, SettingViewModel>(), SettingNavigator, OnItemSelectedListener {
    companion object {
        val TAG = SettingFragment::class.java.simpleName

        fun newInstance() : SettingFragment = SettingFragment()
    }

    @Inject
    internal lateinit var settingViewModel: SettingViewModel
    private var checkSelect = 0

    private var fragmentSettingBinding: FragmentSettingBinding? = null

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.fragment_setting

    override fun getViewModel(): SettingViewModel = settingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        settingViewModel.navigator = this
    }

    override fun onResume() {
        checkSelect = 0
        super.onResume()
    }

    override fun onFragmentReady() {
        fragmentSettingBinding = getViewDataBinding()
        settingViewModel.fetchCountries()

        fragmentSettingBinding?.let {
            it.spinCountry.setSelection(0, false)
            it.spinCountry.onItemSelectedListener = this
        }
        settingViewModel.getCountriesLiveData().observe(this, Observer {
            it?.let {
                settingViewModel.addCountryItemToList(it)
                val position = it.indexOfFirst { it.isDefault }
                fragmentSettingBinding?.let {
                    it.spinCountry.setSelection(position)
                }
            }
        })
    }

    override fun successUpdate(message: String) {
        toast(message)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (position >= 0 && ++checkSelect > 1)
            settingViewModel.setUpSpinnerSelect(position)
    }
}