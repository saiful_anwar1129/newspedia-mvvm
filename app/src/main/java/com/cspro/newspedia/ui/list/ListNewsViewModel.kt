package com.cspro.newspedia.ui.list

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import com.cspro.newspedia.app.AppConst
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.data.model.api.response.ListArticlesResponse
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.ui.detail.DetailNewsNavigator
import com.cspro.newspedia.utils.SchedulerProvider
import com.cspro.newspedia.utils.diffPublishedHour
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class ListNewsViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<ListNewsNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable) {
    private val articleList: ObservableList<Article>
    private val articleListLiveData: MutableLiveData<List<Article>>

    private val articleTopHeadlines: ObservableList<Article>
    private val articleTopHeadlinesLiveData: MutableLiveData<List<Article>>

    init {
        this.articleList = ObservableArrayList<Article>()
        this.articleListLiveData = MutableLiveData()
        this.articleTopHeadlines = ObservableArrayList<Article>()
        this.articleTopHeadlinesLiveData = MutableLiveData()
    }

    fun fetchData(sourceId: String) {
        if (navigator.isNetworkConnected()) {
            setIsLoading(true)

            compositeDisposable.add(zipFetch(sourceId)
                .compose(schedulerProvider.ioToMainFlowableScheduler())
                .doOnTerminate { setIsLoading(false) }
                .subscribe(
                    {
                        articleListLiveData.value = it.first
                        articleTopHeadlinesLiveData.value = it.second
                    },
                    { err -> navigator.onUnknownError(err.message) }
                ))
        } else navigator.onConnectionError()
    }

    fun fetchEverything(sourceId: String) {
        if (navigator.isNetworkConnected()) {
            setIsLoading(true)

            compositeDisposable.add(everythingFlowable(sourceId)
                .compose(schedulerProvider.ioToMainFlowableScheduler())
                .doOnTerminate {
                    navigator.onRefreshStop()
                    setIsLoading(false)
                }
                .subscribe(
                    { articleListLiveData.value = it },
                    {
                        articleList.clear()
                        navigator.onUnknownError(it.message)
                    }
                ))
        } else navigator.onConnectionError()
    }

    private fun zipFetch(sourceId: String) : Flowable<Pair<List<Article>, List<Article>>> =
        Flowable.zip(everythingFlowable(sourceId)
            .observeOn(schedulerProvider.getMainThreadScheduler()),
            topHeadlinesFlowable(sourceId)
                .observeOn(schedulerProvider.getMainThreadScheduler()),
            BiFunction { t1, t2 -> Pair(t1, t2) })

    private fun everythingFlowable(sourceId: String) : Flowable<List<Article>> =
        dataManager.getEverything(sourceId = sourceId)
            .subscribeOn(schedulerProvider.getIOThreadScheduler())
            .doOnNext { if (!it.isSuccessful()) RuntimeException(it.message) }
            .flatMap { Flowable.fromIterable(it.data) }
            .map { article -> article.apply {
                publishedAt = diffPublishedHour(article.publishedAt)
            } }
            .toList()
            .toFlowable()

    private fun topHeadlinesFlowable(sourceId: String) : Flowable<List<Article>> =
        dataManager.getTopHeadlines(sourceId = sourceId)
            .subscribeOn(schedulerProvider.getIOThreadScheduler())
            .doOnNext { if (!it.isSuccessful()) RuntimeException(it.message) }
            .flatMap { Flowable.fromIterable(it.data) }
            .take(AppConst.MAX_LIMIT_PROMO)
            .map { article -> article.apply {
                publishedAt = diffPublishedHour(article.publishedAt, " jam yang lalu")
            } }
            .toList()
            .toFlowable()

    fun addItemsToArticle(list: List<Article>) {
        articleList.clear()
        articleList.addAll(list)
    }

    fun addItemsToArticleTopHeadlines(list: List<Article>) {
        articleTopHeadlines.clear()
        articleTopHeadlines.addAll(list)
    }

    fun getArticleList() : ObservableList<Article> = articleList

    fun getArticleListLiveData() : MutableLiveData<List<Article>> = articleListLiveData

    fun getArticleTopHeadlinesLiveData() : MutableLiveData<List<Article>> = articleTopHeadlinesLiveData

}
