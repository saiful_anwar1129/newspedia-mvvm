package com.cspro.newspedia.ui.main

import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class MainViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<MainNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable)
