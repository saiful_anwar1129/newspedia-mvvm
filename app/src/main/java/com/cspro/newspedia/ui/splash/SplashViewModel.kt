package com.cspro.newspedia.ui.splash

import com.cspro.newspedia.app.AppConst
import com.cspro.newspedia.app.AppLogger
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.ui.base.BaseViewModel
import com.cspro.newspedia.utils.SchedulerProvider
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.lang.RuntimeException
import javax.inject.Inject

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class SplashViewModel @Inject constructor(dataManager: DataManager, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) : BaseViewModel<SplashNavigator>(dataManager = dataManager, schedulerProvider = schedulerProvider, compositeDisposable = disposable) {

    fun startSeeding() {
        compositeDisposable.add(dataManager
            .seedDatabaseCountry()
            .flatMap {
                if(it) {
                    dataManager.loadCountriesByCode(AppConst.DEFAULT_COUNTRY)
                        .concatMap { countries ->
                            if (countries.isNotEmpty()) {
                                getSources(countries.first())
                            } else throw RuntimeException("Data country is empty")
                        }
                } else Observable.just(it)
            }
            .doOnTerminate { setIsLoading(false) }
            .compose(schedulerProvider.ioToMainObservableScheduler())
            .subscribe(
                { decideNextActivity() },
                { error ->
                    AppLogger.d("${error.message}")
                    navigator.onUnknownError(error.message)
                }
            ))
    }

    fun decideNextActivity() {
        navigator.openMainActivity()
    }
}