package com.cspro.newspedia.ui.main.topheadlines

import android.arch.lifecycle.ViewModelProvider
import android.support.v7.widget.LinearLayoutManager
import com.cspro.newspedia.app.ViewModelProviderFactory
import com.cspro.newspedia.data.DataManager
import com.cspro.newspedia.ui.headline.HeadlinePagerAdapter
import com.cspro.newspedia.utils.SchedulerProvider
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Module
class TopHeadlinesFragmentModule{

    @Provides
    internal fun topHeadlinesProvider(viewModel: TopHeadlinesViewModel) : ViewModelProvider.Factory =
        ViewModelProviderFactory(viewModel)

    @Provides
    internal fun provideTopHeadlinesViewModel(dataManager: DataManager, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : TopHeadlinesViewModel =
        TopHeadlinesViewModel(dataManager, schedulerProvider, compositeDisposable)

    @Provides
    internal fun provideTopHeadlinesAdapter() : TopHeadlinesAdapter = TopHeadlinesAdapter(ArrayList())

    @Provides
    fun provideHeadlinePagerAdapter(gson: Gson, topHeadlinesFragment: TopHeadlinesFragment) : HeadlinePagerAdapter =
        HeadlinePagerAdapter(ArrayList(), gson, topHeadlinesFragment.childFragmentManager)
}