package com.cspro.newspedia.ui.search

import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.cspro.newspedia.BR
import com.cspro.newspedia.R
import com.cspro.newspedia.databinding.ActivitySearchNewsBinding
import com.cspro.newspedia.ui.base.BaseActivity
import com.cspro.newspedia.ui.detail.DetailNewsIntent
import org.jetbrains.anko.appcompat.v7.expandedMenuView
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
class SearchNewsActivity : BaseActivity<ActivitySearchNewsBinding, SearchNewsViewModel>(), SearchNewsNavigator, SearchNewsAdapter.SearchAdapterListener{

    @Inject
    internal lateinit var mViewModelFactory: ViewModelProvider.Factory
    @Inject
    internal lateinit var layoutManager: LinearLayoutManager
    @Inject
    internal lateinit var searchNewsAdapter: SearchNewsAdapter

    internal lateinit var searchNewsViewModel: SearchNewsViewModel
    private lateinit var activitySearchBinding: ActivitySearchNewsBinding
    private lateinit var sourceId: String

    override fun getBindingVariable(): Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_search_news

    override fun getViewModel(): SearchNewsViewModel {
        searchNewsViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SearchNewsViewModel::class.java)
        return searchNewsViewModel
    }

    override fun onActivityReady(savedInstanceState: Bundle?) {
        sourceId = intent.getStringExtra(PARAM_SOURCE_ID)
        requireNotNull(sourceId) { "sourceId provided in Intent extras" }

        activitySearchBinding = getViewDataBinding()
        searchNewsViewModel.navigator = this

        layoutManager.orientation = LinearLayoutManager.VERTICAL
        activitySearchBinding?.let {
            setSupportActionBar(it.toolbar)
            searchNewsAdapter.setListener(this)
            it.recyclerView.itemAnimator = DefaultItemAnimator()
            it.recyclerView.layoutManager = layoutManager
            it.recyclerView.adapter = searchNewsAdapter
        }

        subscribeToLiveData()
    }

    private fun subscribeToLiveData() {
        searchNewsViewModel.getArticleListLiveData().observe(this, Observer {
            it?.let { searchNewsViewModel.addItemsToArticle(it) }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = ((menu?.findItem(R.id.searchMenu))?.apply {
            expandActionView()
            setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
                override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                    return true
                }

                override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                    runOnUiThread {
                        hideKeyboard()
                        Thread.sleep(350)
                        onBackPressed()
                    }
                    return true
                }
            })
        }?.actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            expandedMenuView()
        }
        (searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text) as EditText).apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                setTextColor(resources.getColor(R.color.white, theme))
                setHintTextColor(resources.getColor(R.color.white, theme))
            } else {
                setTextColor(resources.getColor(R.color.white))
                setHintTextColor(resources.getColor(R.color.white))
            }
        }

        searchNewsViewModel.performSearhView(searchView, sourceId)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onItemClick(articleUrl: String?, articleTitle: String?) {
        startActivity(baseContext.DetailNewsIntent(articleUrl, articleTitle))
        overridePendingTransition(R.anim.slide_animation_enter, R.anim.nothing)
    }

    override fun onRetryClick() {
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.nothing, R.anim.slide_animation_leave)
    }
}

val PARAM_SOURCE_ID = "PARAM_SOURCE_ID"
fun Context.SearchIntent(sourceId: String = "") : Intent = Intent(this, SearchNewsActivity::class.java).apply {
    putExtra(PARAM_SOURCE_ID, sourceId)
}