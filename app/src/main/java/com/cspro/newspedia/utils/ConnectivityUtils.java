package com.cspro.newspedia.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
public final class ConnectivityUtils {

  private ConnectivityUtils() {
  }

  public static boolean isConnectedToInternet(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context
        .getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo ni = cm.getActiveNetworkInfo();
    return ni != null && ni.isConnected() && ni.isAvailable();
  }

  public static boolean isWifiEnabled(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context
        .getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo ni = cm.getActiveNetworkInfo();
    if (ni != null && ni.getType() == ConnectivityManager.TYPE_WIFI) {
      return true;
    }
    return false;
  }

  public static boolean isMobileDataEnabled(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context
        .getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo ni = cm.getActiveNetworkInfo();
    if (ni != null && ni.getType() == ConnectivityManager.TYPE_MOBILE) {
      return true;
    }
    return false;
  }

}
