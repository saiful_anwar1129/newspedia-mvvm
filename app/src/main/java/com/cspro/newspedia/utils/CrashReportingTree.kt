package com.cspro.newspedia.utils

import android.util.Log
import timber.log.Timber

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class CrashReportingTree : Timber.Tree() {

    override fun isLoggable(tag: String?, priority: Int): Boolean = priority >= Log.ERROR

    override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {
        /*Crashlytics.log(priority, tag, message)

        if (throwable != null) {
            Crashlytics.logException(throwable)
        }*/
    }

}
