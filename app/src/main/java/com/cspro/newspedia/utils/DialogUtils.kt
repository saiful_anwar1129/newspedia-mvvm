package com.cspro.newspedia.utils

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window

import android.view.Gravity
import com.cspro.newspedia.R
import kotlinx.android.synthetic.main.view_loading.*

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class DialogUtils : DialogFragment() {
    private var message: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.view_loading, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        avi_loading.smoothToShow()

        message = arguments?.getString("message")
        loading_message.visibility = if (message.isNullOrEmpty()) View.GONE else View.VISIBLE
    }

    override fun dismiss() {
        avi_loading?.let {
            it.smoothToHide()
        }
        super.dismiss()
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.apply {
                window?.let {
                    it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                    it.setGravity(Gravity.CENTER)
                    it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                }
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        isCancelable = false
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogSlideAnim)
        return dialog
    }

    override fun toString(): String {
        return "DialogUtils"
    }

    companion object {

        fun newInstance(): DialogUtils {
            return DialogUtils().apply {
                val args = Bundle()

                arguments = args
            }
        }

        fun newInstance(message: String): DialogUtils {
            return DialogUtils().apply {
                val args = Bundle()

                args.putString("message", message)
                arguments = args
            }
        }
    }
}
