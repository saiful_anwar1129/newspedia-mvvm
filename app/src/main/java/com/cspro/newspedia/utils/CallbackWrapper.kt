package com.cspro.newspedia.utils

import com.cspro.newspedia.app.AppLogger
import com.cspro.newspedia.data.model.api.response.base.BaseResponse
import com.cspro.newspedia.ui.base.BaseNavigator
import org.json.JSONObject
import java.io.IOException
import java.net.SocketTimeoutException
import io.reactivex.observers.DisposableObserver
import okhttp3.ResponseBody
import retrofit2.HttpException
import timber.log.Timber

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
abstract class CallbackWrapper<T>(private val navigator: BaseNavigator) : DisposableObserver<T>() {

    override fun onNext(t: T) {
        handleOnNext(t)
    }

    override fun onError(t: Throwable) {
        handleError(t)
    }

    override fun onComplete() {
    }

    protected abstract fun onSuccess(response: T)

    private fun getErrorMessage(responseBody: ResponseBody?): String {
        try {
            val jsonObject = JSONObject(responseBody!!.string())
            return jsonObject.getString("message")
        } catch (e: Exception) {
            return e.message!!
        }

    }

    fun handleOnNext(t: T) {
        if (t is BaseResponse<*>) {
            AppLogger.d("isSuccessful: ${t.isSuccessful()}, code: ${t.code}")
            if (t.isSuccessful()) {
                onSuccess(t)
            } else {
                if (t.message != null) navigator.onUnknownError(t.message) else navigator.onUnknownError("onUnknownError")
            }
        } else {
            onSuccess(t)
        }
    }

    fun handleError(t: Throwable) {
        Timber.d("onError(): ${t.message}")
        if (t is HttpException) {
            val responseBody = t.response().errorBody()
            navigator.onUnknownError(getErrorMessage(responseBody))
        } else if (t is SocketTimeoutException) {
            navigator.onTimeout()
        } else if (t is IOException) {
            navigator.onNetworkError()
        } else {
            with(t.message) {
                when {
                    t.message == null -> navigator.onUnknownError("onUnknownError")
                    else -> navigator.onUnknownError(t.message!!)
                }
            }
        }
    }
}