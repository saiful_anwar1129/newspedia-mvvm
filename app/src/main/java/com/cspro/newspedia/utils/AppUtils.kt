package com.cspro.newspedia.utils

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.SearchView
import com.cspro.newspedia.app.AppConst
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.absoluteValue
import android.content.res.AssetManager
import java.io.IOException
import java.nio.charset.Charset


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class Time(var hours: Int, var minutes: Int, var seconds: Int)

fun differenceTime(start: Time, stop: Time): Time {
    val diff = Time(0, 0, 0)

    if (stop.seconds > start.seconds) {
        --start.minutes
        start.seconds += 60
    }

    diff.seconds = start.seconds - stop.seconds
    if (stop.minutes > start.minutes) {
        --start.hours
        start.minutes += 60
    }

    diff.minutes = start.minutes - stop.minutes
    diff.hours = start.hours - stop.hours

    return diff
}

inline fun <T: Fragment> T.withArgs(
        argsBuilder: Bundle.() -> Unit): T =
        this.apply {
            arguments = Bundle().apply(argsBuilder)
        }

fun fromView(searchView: SearchView): Observable<String> {

    val subject = PublishSubject.create<String>()

    searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(s: String): Boolean {
            subject.onNext(s)
            return true
        }

        override fun onQueryTextChange(text: String): Boolean {
            subject.onNext(text)
            return true
        }
    })

    return subject
}

fun diffPublishedHour(dateEnd: String, endPrefix: String = "jm") : String {
    val calStart = Calendar.getInstance()
    val calEnd = Calendar.getInstance()

    return try {
        calEnd.time = SimpleDateFormat(AppConst.FORMAT_DATE_2, Locale.US).parse(dateEnd)
        val dateCaculator = differenceTime(
            Time(calStart.get(Calendar.HOUR_OF_DAY), calStart.get(Calendar.MINUTE), calStart.get(Calendar.SECOND)),
            Time(calEnd.get(Calendar.HOUR_OF_DAY), calEnd.get(Calendar.MINUTE), calEnd.get(Calendar.SECOND))
        )
        "${dateCaculator.hours.absoluteValue}$endPrefix"
    } catch (e: ParseException) {
        e.printStackTrace()
        ""
    }
}

fun diffPublishedHourMinutes(dateEnd: String) : String {
    val calStart = Calendar.getInstance()
    val calEnd = Calendar.getInstance()

    return try {
        calEnd.time = SimpleDateFormat(AppConst.FORMAT_DATE_2, Locale.US).parse(dateEnd)
        val dateCaculator = differenceTime(
            Time(calStart.get(Calendar.HOUR_OF_DAY), calStart.get(Calendar.MINUTE), calStart.get(Calendar.SECOND)),
            Time(calEnd.get(Calendar.HOUR_OF_DAY), calEnd.get(Calendar.MINUTE), calEnd.get(Calendar.SECOND))
        )
        "${dateCaculator.hours.absoluteValue}jm ${dateCaculator.minutes}mn"
    } catch (e: ParseException) {
        e.printStackTrace()
        ""
    }

}

fun formatDate(dateEnd: String) : String {
    val calEnd = Calendar.getInstance()

    return try {
        calEnd.time = SimpleDateFormat(AppConst.FORMAT_DATE_2, Locale.US).parse(dateEnd)
        SimpleDateFormat(AppConst.FORMAT_DATE_4, Locale.US).format(calEnd.time)
    } catch (e: ParseException) {
        e.printStackTrace()
        ""
    }
}

@Throws(IOException::class)
fun loadJSONFromAsset(context: Context, jsonFileName: String): String {
    val manager = context.getAssets()
    val iS = manager.open(jsonFileName)

    val size = iS.available()
    val buffer = ByteArray(size)
    iS.read(buffer)
    iS.close()

    return String(buffer, Charset.forName("UTF-8"))
}
