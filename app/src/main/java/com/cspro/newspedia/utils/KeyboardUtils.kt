package com.cspro.newspedia.utils

import android.widget.EditText
import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
object KeyboardUtils {

    /**
     * Shows the keyboard.
     */
    fun showKeyboard(activity: Activity, editText: EditText) {
        val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (editText.requestFocus()) {
            inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    /**
     * Hides the keyboard.
     */
    fun hideKeyboard(activity: Activity, editText: EditText) {
        val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(editText.windowToken, 0)
    }

}