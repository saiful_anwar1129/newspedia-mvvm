package com.cspro.newspedia.utils

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.cspro.newspedia.R
import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.data.network.models.Source
import com.cspro.newspedia.ui.main.browse.SourceAdapter
import com.cspro.newspedia.ui.main.topheadlines.TopHeadlinesAdapter
import com.cspro.newspedia.ui.search.SearchNewsAdapter


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
@BindingAdapter("imageUrl")
fun ImageView.loadImage(url: String?) {
    url?.let {
        Glide.with(this.context)
            .load(url)
            .apply(
                RequestOptions()
                    .apply {
                        centerCrop()
                        placeholder(R.drawable.ic_empty_photo)
                        centerCrop()
                        error(R.drawable.ic_empty_photo)
                    })
            .into(this)
    }
}

@BindingAdapter("adapter")
fun RecyclerView.addArticleItems(articleItems: MutableList<Article>) {
    adapter?.let {
        when(it) {
            is TopHeadlinesAdapter -> {
                it.clearItems()
                it.addItems(articleItems)
            }
            is SearchNewsAdapter -> {
                it.clearItems()
                it.addItems(articleItems)
            }
        }
    }
}

@BindingAdapter("adapter")
fun RecyclerView.addSourceItems(sourceItems: MutableList<Source>) {
    adapter?.let {
        when(it) {
            is SourceAdapter -> {
                it.clearItems()
                it.addItems(sourceItems)
            }
        }
    }
}