package com.cspro.newspedia.data.network.remote

import okhttp3.Interceptor
import okhttp3.Response

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

class DefaultInterceptor constructor(private val apiKey: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val requestBuilder = original.newBuilder()
                .apply {
                    addHeader("Authorization", apiKey)
                }
                .method(original.method(), original.body())

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}