package com.cspro.newspedia.data.model.api.response.base

import timber.log.Timber


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

abstract class BaseResponse<T>{
    abstract var status: String

    abstract var code: String?

    abstract var message: String?

    abstract var data: T?

    fun isSuccessful() : Boolean = status.equals("ok")
}