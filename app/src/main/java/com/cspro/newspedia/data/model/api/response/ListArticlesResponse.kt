package com.cspro.newspedia.data.model.api.response

import com.cspro.newspedia.data.model.api.response.base.BaseResponse
import com.cspro.newspedia.data.network.models.Article
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
data class ListArticlesResponse(
    @Expose
    @SerializedName(value = "status")
    override var status: String,

    @Expose
    @SerializedName(value = "code")
    override var code: String?,

    @Expose
    @SerializedName(value = "message")
    override var message: String? = null,

    @Expose
    @SerializedName(value = "totalResults")
    var totalResults: Long,

    @Expose
    @SerializedName(value = "articles")
    override var data: List<Article>?
) : BaseResponse<List<Article>>()