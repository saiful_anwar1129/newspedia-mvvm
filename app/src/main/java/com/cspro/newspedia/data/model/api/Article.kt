package com.cspro.newspedia.data.network.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
data class Article(@Expose
                   @SerializedName("author")
                   var author: String? = null,

                   @Expose
                   @SerializedName("title")
                   var title: String,

                   @Expose
                   @SerializedName("description")
                   var description: String? = null,

                   @Expose
                   @SerializedName("url")
                   var url: String,

                   @Expose
                   @SerializedName("urlToImage")
                   var urlToImage: String? = null,

                   @Expose
                   @SerializedName("publishedAt")
                   var publishedAt: String,

                   @Expose
                   @SerializedName("content")
                   var content: String? = null,

                   @Expose
                   @SerializedName("source")
                   var source: Source? = null)