package com.cspro.newspedia.data.network.api

import com.cspro.newspedia.data.network.api.topheadlines.TopHeadlinesApi
import com.cspro.newspedia.data.remote.everything.EverythingApi
import com.cspro.newspedia.data.remote.source.SourceApi

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

interface ApiHelper : TopHeadlinesApi, EverythingApi, SourceApi {

}