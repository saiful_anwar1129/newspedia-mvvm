package com.cspro.newspedia.data.network.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
data class Source(@Expose
                  @SerializedName("id")
                  var id: String,

                  @Expose
                  @SerializedName("name")
                  var name: String,

                  @Expose
                  @SerializedName("description")
                  var description: String,

                  @Expose
                  @SerializedName("url")
                  var url: String,

                  @Expose
                  @SerializedName("category")
                  var category: String,

                  @Expose
                  @SerializedName("language")
                  var language: String,

                  @Expose
                  @SerializedName("country")
                  var country: String)