package com.cspro.newspedia.data.local.db.dao

import android.arch.persistence.room.*
import com.cspro.newspedia.data.model.db.Country


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Dao
interface CountryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(countries: List<Country>) : List<Long>

    @Query("SELECT * FROM country ORDER BY name ASC")
    fun loadAll() : List<Country>

    @Query("SELECT * FROM country WHERE code = :code")
    fun loadByCode(code: String) : List<Country>

    @Query("SELECT * FROM country WHERE is_default = :isDefault")
    fun loadIsDefault(isDefault: Boolean = true) : List<Country>

    @Query("UPDATE country SET is_default = CASE WHEN id = :id THEN :currentTrue ELSE :currentFalse END")
    fun updateToDefault(currentTrue: Boolean = true, currentFalse: Boolean = false, id: Long) : Int

    @Update
    fun update(vararg country: Country) : Int
}