package com.cspro.newspedia.data.network.api.topheadlines

import retrofit2.Retrofit
import javax.inject.Inject

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

class TopHeadlinesAppApi() : TopHeadlinesApi {

    private lateinit var topHeadlinesService: TopHeadlinesService

    @Inject
    constructor(retrofit: Retrofit) : this() {
        this.topHeadlinesService = retrofit.create(TopHeadlinesService::class.java)
    }

    override fun getTopHeadlines(country: String?, sourceId: String?) = topHeadlinesService.getTopHeadlinesApiCall(country, sourceId)
}