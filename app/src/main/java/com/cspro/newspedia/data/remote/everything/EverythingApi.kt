package com.cspro.newspedia.data.remote.everything

import com.cspro.newspedia.data.model.api.response.ListArticlesResponse
import io.reactivex.Flowable
import io.reactivex.Observable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
interface EverythingApi {

    fun getEverything(query: String? = null,
                      sourceId: String? = null,
                      sortBy: String? = null) : Flowable<ListArticlesResponse>

}