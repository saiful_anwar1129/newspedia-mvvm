package com.cspro.newspedia.data.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/27/2019.
 */
@Entity(tableName = "country")
data class Country(
    @Expose
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,

    @Expose
    @SerializedName("name")
    @ColumnInfo(name = "name")
    var name: String,

    @Expose
    @SerializedName("code")
    @ColumnInfo(name = "code")
    var code: String,

    @Expose
    @SerializedName("is_default")
    @ColumnInfo(name = "is_default")
    var isDefault: Boolean = false,

    @Expose
    @SerializedName("sources")
    @ColumnInfo(name = "sources")
    var sources: String? = null) {

    override fun toString(): String {
        return "${this.name} - ${this.code.toUpperCase()}"
    }
}