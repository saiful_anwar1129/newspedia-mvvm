package com.cspro.newspedia.data.local.db

import com.cspro.newspedia.data.model.db.Country
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class AppDbHelper @Inject internal constructor(private val appDatabase: AppDatabase): DbHelper {

    override fun clearAllTable(): Observable<Boolean> =
        Observable.fromCallable {
            appDatabase.clearAllTables()
            return@fromCallable true
        }

    override fun insertAllCountries(countries: List<Country>): Maybe<List<Long>> =
        Maybe.fromCallable {
            appDatabase.countryDao().insertAll(countries)
        }


    override fun loadAllCountries(): Observable<List<Country>> =
        Observable.fromCallable {
            appDatabase.countryDao().loadAll()
        }

    override fun loadCountriesByCode(code: String): Observable<List<Country>> =
        Observable.fromCallable {
            appDatabase.countryDao().loadByCode(code)
        }

    override fun loadCountriesIsDefault(): Observable<List<Country>> =
        Observable.fromCallable {
            appDatabase.countryDao().loadIsDefault()
        }

    override fun updateCountry(country: Country): Maybe<Int> =
        Maybe.fromCallable {
            appDatabase.countryDao().update(country)
        }

    override fun updateToDefaultCountry(countryId: Long): Maybe<Int> =
        Maybe.fromCallable {
            appDatabase.countryDao().updateToDefault(id = countryId)
        }

    override fun isCountryEmpty(): Observable<Boolean> =
        Observable.fromCallable {
                appDatabase.countryDao().loadAll().isEmpty()
            }
}