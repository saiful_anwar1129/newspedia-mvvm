package com.cspro.newspedia.data.network.remote

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

object ApiEndPoint {
    const val TOP_HEADLINES_URL = "top-headlines"
    const val EVERYTHING_URL = "everything"
    const val SOURCES_URL = "sources"
}