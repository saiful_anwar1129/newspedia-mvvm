package com.cspro.newspedia.data.local.prefs

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import com.cspro.newspedia.di.qualifiers.PreferenceInfo
import com.google.gson.Gson
import io.reactivex.Observable
import javax.inject.Inject

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class AppPreferenceHelper @Inject constructor(context: Context,
                                              @PreferenceInfo private val prefFileName: String, internal val gson: Gson) : PreferenceHelper {

    override fun clear(): Observable<Boolean> {
        return Observable.fromCallable<Boolean> {
            mPrefs.edit{ clear() }
            true
        }
    }

    override fun remove(key: String): Observable<Boolean> {
        return Observable.fromCallable<Boolean> {
            mPrefs.edit { remove(key) }
            true
        }
    }

    private val mPrefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    companion object {
        private val PREF_KEY_HAS_SEEDING = "PREF_KEY_HAS_SEEDING"
    }

}