package com.cspro.newspedia.data.local.prefs

import io.reactivex.Observable

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
interface PreferenceHelper {
    fun clear(): Observable<Boolean>

    fun remove(key: String): Observable<Boolean>
}