package com.cspro.newspedia.data.network.api.topheadlines

import com.cspro.newspedia.data.model.api.response.ListArticlesResponse
import io.reactivex.Flowable
import io.reactivex.Observable

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

interface TopHeadlinesApi {

    fun getTopHeadlines(country: String? = null,
                        sourceId: String? = null) : Flowable<ListArticlesResponse>

}