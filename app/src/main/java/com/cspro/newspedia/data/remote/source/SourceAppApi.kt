package com.cspro.newspedia.data.remote.source

import com.cspro.newspedia.data.model.api.response.ListSourcesResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class SourceAppApi() : SourceApi{

    private lateinit var sourService: SourceService

    @Inject
    constructor(retrofit: Retrofit) : this() {
        this.sourService = retrofit.create(SourceService::class.java)
    }

    override fun getSources(country: String?): Observable<ListSourcesResponse> = sourService.getSourceApiCall(country)
}