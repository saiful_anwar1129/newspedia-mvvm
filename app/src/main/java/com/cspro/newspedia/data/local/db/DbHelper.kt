package com.cspro.newspedia.data.local.db

import com.cspro.newspedia.data.model.db.Country
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Singleton

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
@Singleton
interface DbHelper {

    fun clearAllTable() : Observable<Boolean>

    fun insertAllCountries(countries: List<Country>) : Maybe<List<Long>>

    fun loadAllCountries() : Observable<List<Country>>

    fun loadCountriesByCode(code: String) : Observable<List<Country>>

    fun loadCountriesIsDefault() : Observable<List<Country>>

    fun isCountryEmpty() : Observable<Boolean>

    fun updateCountry(country: Country) : Maybe<Int>

    fun updateToDefaultCountry(countryId: Long) : Maybe<Int>

}