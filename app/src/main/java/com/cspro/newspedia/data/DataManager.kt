package com.cspro.newspedia.data

import com.cspro.newspedia.data.network.api.ApiHelper
import com.cspro.newspedia.data.local.db.DbHelper
import com.cspro.newspedia.data.local.prefs.PreferenceHelper
import io.reactivex.Observable
import io.reactivex.Single

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    05/06/2018.
 */

interface DataManager : PreferenceHelper, DbHelper, ApiHelper {

    fun seedDatabaseCountry() : Observable<Boolean>

}