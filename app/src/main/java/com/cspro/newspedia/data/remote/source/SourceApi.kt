package com.cspro.newspedia.data.remote.source

import com.cspro.newspedia.data.model.api.response.ListSourcesResponse
import io.reactivex.Observable


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
interface SourceApi {

    fun getSources(country: String? = null) : Observable<ListSourcesResponse>

}