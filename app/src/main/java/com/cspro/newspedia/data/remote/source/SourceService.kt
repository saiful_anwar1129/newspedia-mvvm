package com.cspro.newspedia.data.remote.source

import com.cspro.newspedia.data.model.api.response.ListSourcesResponse
import com.cspro.newspedia.data.network.remote.ApiEndPoint
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
interface SourceService {

    @GET(ApiEndPoint.SOURCES_URL)
    fun getSourceApiCall(@Query("country") country: String?) : Observable<ListSourcesResponse>

}