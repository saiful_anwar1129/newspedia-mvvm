package com.cspro.newspedia.data

import android.content.Context
import com.cspro.newspedia.app.AppConst
import com.cspro.newspedia.app.AppLogger
import com.cspro.newspedia.data.model.api.response.ListArticlesResponse
import com.cspro.newspedia.data.network.api.ApiHelper
import com.cspro.newspedia.data.local.db.DbHelper
import com.cspro.newspedia.data.local.prefs.PreferenceHelper
import com.cspro.newspedia.data.model.api.response.ListSourcesResponse
import com.cspro.newspedia.data.model.db.Country
import com.cspro.newspedia.utils.loadJSONFromAsset
import com.google.gson.Gson
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import com.google.gson.reflect.TypeToken



/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class AppDataManager() : DataManager {

    private lateinit var context: Context

    private lateinit var gson: Gson

    private lateinit var apiHelper: ApiHelper

    private lateinit var preferenceHelper: PreferenceHelper

    private lateinit var dbHelper: DbHelper

    @Inject
    constructor(context: Context, gson: Gson, preferenceHelper: PreferenceHelper, dbHelper: DbHelper, apiHelper: ApiHelper) : this() {
        this.context = context
        this.gson = gson
        this.apiHelper = apiHelper
        this.preferenceHelper = preferenceHelper
        this.dbHelper = dbHelper
    }

    /**
     * API HELPER
     *
     * [--START--]
     */

    override fun getTopHeadlines(country: String?, sourceId: String?): Flowable<ListArticlesResponse> =
            apiHelper.getTopHeadlines(country, sourceId)

    override fun getEverything(query: String?, sourceId: String?, sortBy: String?): Flowable<ListArticlesResponse> =
            apiHelper.getEverything(query, sourceId, sortBy)

    override fun getSources(country: String?): Observable<ListSourcesResponse> =
            apiHelper.getSources(country)


    /**
     * API HELPER
     *
     * [--END--]
     */
    
    
    /**
     * PREFERENCE HELPER
     *
     * [--START--]
     */

    override fun clear(): Observable<Boolean> = preferenceHelper.clear()

    override fun remove(key: String): Observable<Boolean> = preferenceHelper.remove(key)

    /**
     * PREFERENCE HELPER
     *
     * [--END--]
     */


    /**
     * DATABASE HELPER
     *
     * [--START--]
     */


    override fun clearAllTable(): Observable<Boolean> = dbHelper.clearAllTable()

    override fun loadAllCountries(): Observable<List<Country>> = dbHelper.loadAllCountries()

    override fun insertAllCountries(countries: List<Country>): Maybe<List<Long>> = dbHelper.insertAllCountries(countries)

    override fun isCountryEmpty(): Observable<Boolean> = dbHelper.isCountryEmpty()

    override fun loadCountriesIsDefault(): Observable<List<Country>> = dbHelper.loadCountriesIsDefault()

    override fun loadCountriesByCode(code: String): Observable<List<Country>> = dbHelper.loadCountriesByCode(code)

    override fun updateCountry(country: Country): Maybe<Int> = dbHelper.updateCountry(country)

    override fun updateToDefaultCountry(countryId: Long): Maybe<Int> = dbHelper.updateToDefaultCountry(countryId)

    override fun seedDatabaseCountry(): Observable<Boolean> = dbHelper.isCountryEmpty()
        .concatMap { isEmpty ->
            if (isEmpty) {
                AppLogger.d("A")
                val type = object : TypeToken<List<Country>>() {}.type
                val countryList: List<Country> = gson.fromJson(loadJSONFromAsset(context, AppConst.SEED_DATABASE_COUNTRIES), type)
                insertAllCountries(countryList.map { it -> it.apply { code = it.code.toLowerCase() } })
                    .flatMapObservable { Observable.just(true) }
            } else Observable.just(false)
        }

}