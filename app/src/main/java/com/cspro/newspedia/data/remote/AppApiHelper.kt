package com.cspro.newspedia.data.network.api

import com.cspro.newspedia.data.model.api.response.ListArticlesResponse
import com.cspro.newspedia.data.model.api.response.ListSourcesResponse
import com.cspro.newspedia.data.network.api.topheadlines.TopHeadlinesApi
import com.cspro.newspedia.data.remote.everything.EverythingApi
import com.cspro.newspedia.data.remote.source.SourceApi
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

class AppApiHelper @Inject constructor(private val topHeadlinesApi: TopHeadlinesApi,
                                       private val everythingApi: EverythingApi,
                                       private val sourceApi: SourceApi) : ApiHelper {

    override fun getTopHeadlines(country: String?, sourceId: String?): Flowable<ListArticlesResponse> =
        topHeadlinesApi.getTopHeadlines(country, sourceId)

    override fun getEverything(query: String?, sourceId: String?, sortBy: String?): Flowable<ListArticlesResponse> =
        everythingApi.getEverything(query, sourceId, sortBy)

    override fun getSources(country: String?): Observable<ListSourcesResponse> =
        sourceApi.getSources(country)
}