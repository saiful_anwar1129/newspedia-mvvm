package com.cspro.newspedia.data.network.remote

import com.cspro.newspedia.BuildConfig

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */

object BaseUrl {
    const val PROTOCOL_HTTPS = "https://"
    const val PROTOCOL_HTTP = "http://"
    const val API_ENDPOINT = BuildConfig.BASE_URL
    const val PORT = ""
    const val VERSION = "v2/"
    const val OAUTH_ENDPOINT = "oauth/"

    fun getBaseUrl() = PROTOCOL_HTTPS.plus(API_ENDPOINT).plus(if(PORT.isNotBlank()) ":" else "").plus(PORT).plus("/")

    fun getVersion() = getBaseUrl().plus(VERSION)
}