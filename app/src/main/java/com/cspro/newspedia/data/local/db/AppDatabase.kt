package com.cspro.newspedia.data.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.cspro.newspedia.data.local.db.dao.CountryDao
import com.cspro.newspedia.data.model.db.Country

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
@Database(entities = arrayOf(
    Country::class
), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun countryDao() : CountryDao

}