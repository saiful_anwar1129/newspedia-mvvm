package com.cspro.newspedia.data.remote.everything

import com.cspro.newspedia.data.model.api.response.ListArticlesResponse
import io.reactivex.Flowable
import io.reactivex.Observable
import retrofit2.Retrofit
import javax.inject.Inject


/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/28/2019.
 */
class EverythingAppApi() : EverythingApi {

    private lateinit var everythingService: EverythingService

    @Inject
    constructor(retrofit: Retrofit) : this() {
        this.everythingService = retrofit.create(EverythingService::class.java)
    }

    override fun getEverything(query: String?, sourceId: String?, sortBy: String?): Flowable<ListArticlesResponse> =
            everythingService.getEverything(query, sourceId, sortBy)
}