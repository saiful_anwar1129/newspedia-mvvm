package com.cspro.newspedia.app

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
object AppConst {
    val TAG = "newspedia"
    val DEFAULT_COUNTRY = "us"
    val SEED_DATABASE_COUNTRIES= "seed/countries.json"
    val RETRY_NETWORK_REQUEST_COUNT = 3
    val TIMES_LOOP_PROMO = 5000L
    val NULL_INDEX = -1L
    val MAX_LIMIT_PROMO = 9L
    val PREF_NAME = "newspedia_pref"
    val DB_NAME = "newspedia.db"
    val FORMAT_DATE_1 = "yyyy-MM-dd HH:mm:ss"
    val FORMAT_DATE_2 = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    val FORMAT_DATE_3 = "EEEE, d MMMM yyyy"
    val FORMAT_DATE_4 = "d MMMM yyyy, h:mm a"

    enum class LoggedInMode private constructor(val type: Int) {
        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_SERVER(1)
    }
}