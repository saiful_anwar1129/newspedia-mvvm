package com.cspro.newspedia.app

import android.app.Activity
import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import com.cspro.newspedia.BuildConfig
import com.cspro.newspedia.di.component.AppComponent
import com.cspro.newspedia.di.component.DaggerAppComponent
import com.cspro.newspedia.di.module.NetworkModule
import com.cspro.newspedia.utils.CrashReportingTree
import com.facebook.stetho.Stetho
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * @author   Saiful Anwar <saiful.anwar1129@gmail.com>
 * @version  1.0
 * @since    1/26/2019.
 */
class NewspediaApp : MultiDexApplication(), HasActivityInjector {

    companion object {
        @JvmStatic lateinit var appComponent: AppComponent
    }

    @Inject
    lateinit var mActivityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun attachBaseContext(base: Context?) {
        MultiDex.install(base);
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        initTimber()

        Stetho.initializeWithDefaults(this)

        initDagger()
    }

    fun initTimber() {
        val timberTree = if (BuildConfig.DEBUG) Timber.DebugTree() else CrashReportingTree()
        Timber.plant(timberTree)
    }

    fun initDagger() {
        appComponent = createComponent()
        appComponent.inject(this)
    }

    fun createComponent(): AppComponent {
        val appComponent = DaggerAppComponent.builder()
                .application(this)
                .networkModule(NetworkModule())
                .build()
        return appComponent
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return mActivityDispatchingAndroidInjector
    }

}