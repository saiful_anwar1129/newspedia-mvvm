package com.cspro.newspedia

import com.cspro.newspedia.data.network.models.Article
import com.cspro.newspedia.utils.diffPublishedHour
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun test() {

        val data: MutableList<Article> = mutableListOf(Article(title = "Juni", url = "", publishedAt = "2019-01-27T23:54:00Z"), Article(title = "Luke", url = "", publishedAt = "2019-01-28T00:37:08Z"))

        Observable.just(data)
            .map { Observable.fromIterable(it) }
            .flatMap { x -> x }
            .map { y ->
                y.apply {
                    title = title.toUpperCase()
                    publishedAt = diffPublishedHour(y.publishedAt)
                    url = y.url
                }
            }
            .toList()
            .toObservable()
            .observeOn(Schedulers.trampoline())
            .subscribeOn(Schedulers.trampoline())
            .subscribe(
                {
                    it.forEach {
                        println(it.title)
                        println(it.publishedAt)
                    }
                }
            )

    }

    @Test
    fun lokal() {
        Observable.just(1, 2, 3, 4)
            .doOnNext { println(it) }
            .filter { x -> x > 20 }
            .observeOn(Schedulers.trampoline())
            .subscribeOn(Schedulers.trampoline())
            .subscribe(
                { println(it) },
                { println("onError") },
                { println("onComplete") }
            )
    }
}
